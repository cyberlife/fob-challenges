const invariant = require('invariant');
const roundTo = require('round-to');
const Maker = require('@makerdao/dai');

const sensible = require("./sensible")

const MIN_ADD_AMOUNT = 0.0001;
const ROUND_TO_PLACES = 4;

const ratioAddition = 1

// Connect to Kovan
const maker = Maker.create('kovan', {
  privateKey: sensible.makerPrivateKey,
  web3: {
    statusTimerDelay: 2000,
    confirmedBlockCount: 2,
    transactionSettings: {
      gasPrice: 30000000000
    }
  },
  log: false
});

async function createCDP(priceFloor, principal) {

  //Check parameters

  invariant(
      priceFloor !== undefined &&
      principal !== undefined,
    'Not all parameters (priceFloor, principal) were received'
  );

  console.log(`CREATING A CDP\n\n`)

  //Declare that the smart agent is starting its work

  console.log('The Maker smart agent is creating a leveraged CDP with the following parameters:');
  console.log(`Price Floor: $${priceFloor}`);
  console.log(`Principal: ${principal} ETH`);

  //Authenticate with Maker

  await maker.authenticate();

  //Get Maker liquidation and ETH price

  const liquidationRatio = await maker.service('cdp').getLiquidationRatio();
  const priceEth = (await maker.service('price').getEthPrice()).toNumber();

  invariant(
    priceEth > priceFloor,
    'Price floor must be below the current oracle price'
  );

  console.log('Opening a CDP...');
  const cdp = await maker.openCdp();
  const id = await cdp.getId();

  // calculate a collateralization ratio that will achieve the given price floor
  const collatRatio = priceEth * liquidationRatio / priceFloor;
  console.log(`Target ratio: ${collatRatio}`);

  // lock up all of our principal
  await cdp.lockEth(principal);
  console.log(`Locked ${principal} ETH`);

  // calculate how much Dai we need to draw in order
  // to achieve the desired collateralization ratio
  let drawAmt = Math.floor(principal * priceEth / collatRatio);
  await cdp.drawDai(drawAmt);

  console.log(`The agent drew ${drawAmt} Dai`);

  return [id, drawAmt];

}

async function topupCDP(id) {

  console.log(`\nTOPPING UP A CDP\n\n`)

  console.log(`The Maker smart agent is now topping up the CDP with id ${id}...`)

  const cdp = await maker.getCdp(id);

  const ratio = await cdp.getCollateralizationRatio();
  console.log(`The ratio right now is ${ratio}`)

  const collateral = await cdp.getCollateralValue();
  const debt = await cdp.getDebtValue(Maker.USD);
  const collateralPrice = await maker.service('price').getEthPrice();

  const targetRatio = ratio + ratioAddition;

  let addAmount = debt
    .times(targetRatio)
    .minus(collateral.times(collateralPrice))
    .div(collateralPrice)
    .toNumber();

  if (addAmount < MIN_ADD_AMOUNT) {
    addAmount = MIN_ADD_AMOUNT;
  } else {
    addAmount = roundTo.up(addAmount, ROUND_TO_PLACES);
  }

  await cdp.lockEth(addAmount);

  const newRatio = await cdp.getCollateralizationRatio();

  console.log(`Done topping up CDP ${id}. The new ratio is ${newRatio}`)

}

module.exports = {

  createCDP,

  topupCDP

}
