var Web3 = require('web3');

var accounts = require('./accounts.js');

//55 Gwei
var DEFAULT_GAS_PRICE = 55000000000
var DEFAULT_GAS_LIMIT = 2500000

async function getWeb3(network, type) {

  if (type == undefined) return undefined;

  if (type == "http") {

    if (network == "local")
      return new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

    else if (network == "thunder")
      return new Web3(new Web3.providers.HttpProvider("https://testnet-rpc.thundercore.com:8544"));

    else if (network == "rinkeby") {

      return new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

    } else if (network == "ropsten") {

      return new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/OPVe8KJGtYczgs2np3R2"));

    }

  } else {

    if (network == "local")
      return new Web3(new Web3.providers.WebsocketProvider("http://localhost:8545"));

    else if (network == "thunder")
      return new Web3(new Web3.providers.WebsocketProvider("https://testnet-rpc.thundercore.com:8544"));

    else if (network == "rinkeby") {

      return new Web3(new Web3.providers.WebsocketProvider("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

    } else if (network == "ropsten") {

      return new Web3(new Web3.providers.WebsocketProvider("https://ropsten.infura.io/OPVe8KJGtYczgs2np3R2"));

    }

  }
}

async function requestLoan(network, instance, pubKey, serializedLoan) {

  instance.methods.requestLoan(serializedLoan).send({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  })
  .on('receipt', (receipt) => {

  })
  .on('error', (error) => {

  })

}

module.exports = {

  getWeb3,

  requestLoan

}
