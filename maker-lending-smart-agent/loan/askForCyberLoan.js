var utils = require("./utils.js");
var contractsAddresses = require("./contractsAddresses.js")
var contractCaller = require("./contractCaller.js");

var principalAddr = "0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE";
var collAddr = contractsAddresses.localContracts[0];

var botBorrower = "0xcecc7c956a1707b22b81179ae67018f7f8ff57f7"

var net = "local"

async function createDAICollateralizedLoan(daiAmount) {

  var web3 = await contractCaller.getWeb3(net, "http");

  var loan = await serializeLoan(web3, daiAmount);

  var loanManagerInstance =
    await utils.getContractInstance(net, "http", "lendingManager")

  console.log("CYBERLIFE LOAN REQUEST\n")
  console.log("The smart agent is using the borrowed "
               + daiAmount.toString()
               + " DAI as collateral for a Cyberlife loan")

  await contractCaller.requestLoan(net, loanManagerInstance, botBorrower, loan)

  console.log("Done requesting the loan. Waiting for lenders to fill...")

}

async function serializeLoan(web3Instance, daiAmount) {

  var collateralDetails = await web3Instance.eth.abi.encodeParameter('uint256', daiAmount.toString());

  var secondsUntilMaturity = 600;

  var collType = 0; //erc20

  var thresholds = 10; //need to pay after every minute

  var interest = 500; //5%

  var principalAmount = '500000000000000'

  var serializedLoan =
  await
  web3Instance.eth.abi
  .encodeParameters(

  ['uint','uint', 'bytes', 'address', 'address', 'uint', 'uint', 'uint'],
  [secondsUntilMaturity.toString(),
  collType.toString(),
  collateralDetails.toString(),
  principalAddr.toString(),
  collAddr.toString(),
  thresholds.toString(),
  interest.toString(),
  principalAmount.toString()]

  );

  return serializedLoan;

}

module.exports = {

  createDAICollateralizedLoan

}
