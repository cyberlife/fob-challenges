# :stars: Future of Blockchain Challenges -- Cyberlife Team :stars:

## Table of Contents

- [Brief Overview](#brief-overview)
- [Maker DAO Lending Smart Agent](#lending-smart-agent)
- [Kyber Crypto Savings Rebalancer](#crypto-savings-rebalancer)
- [Thunder Cashback Markets](#cashback-markets)

## Brief Overview

Although this repository covers three different challenges, these mini projects are integrated into a more holistic vision in the [cyberlife-js](https://gitlab.com/cyberlife/cyberlife-js) repo. The cashback markets and the rebalancer can be run as they are in this repository, but the Maker smart agent needs to be integrated with the rest of the Cyberlife system.

## Lending Smart Agent

The lending smart agent is a piece of software which creates a CDP, draws DAI and uses the DAI as collateral for a [Cyberlife loan](https://gitlab.com/cyberlife/cyberlife-js/tree/master/bank-protocol).

As any Cyberlife smart agent, the lending agent moves between hosts (aka everyday computers) in order to find the ones charging the lowest rent. The agent can use the DAI-collateralized loan to pay for rent and make sure it's not deleted by its hosts.

This autonomous agent addresses the Maker challenge by showing how a self-interested piece of software can interact with CDPs and use the borrowed Dai for subsequent loans. The concept of a lending agent can be extended to cover [blockchain rent](https://github.com/ethereum/EIPs/issues/87) where smart contracts designate off-chain software to get crypto loans for them in case the contracts themselves are almost out of funds and want to avoid getting hypernated.

The post-competition plan for the lending agent is to convert it into a [Keeper](https://developer.makerdao.com/keepers/) that monitors the Maker network and makes money for its creator. Unlike a normal Keeper, the lending agent does not live only on one machine, but on a bunch of machines scattered around the world. Moreover, an agent can be programmed to keep all the revenue for itself and build up wealth :moneybag: :moneybag:

## Crypto Savings Rebalancer

The Kyber savings rebalancer is a component of the [Cyberlife bank protocol](https://gitlab.com/cyberlife/cyberlife-js/tree/master/bank-protocol). The bank protocol :bank: is used as a middleware between cashback markets (a component meant for humans) and the smart agent network (a component meant for software).

The savings rebalancer uses the Kyber network in order to diversify the funds of customers who delegated the Cyberlife bank with handling their wealth. Cyberlife acts as a semi-trusted relayer which detects sudden movements in crypto-asset prices and swaps those coins with the most drastic changes.

Although it's not really a "fund", the rebalancer addresses the Kyber challenge by offering normal, non-crypto people an easy way to diversify their crypto savings by only paying a small per second fee for these services.

The post-competition plan for the savings rebalancer is to integrate it with a mobile app where customers who receive cashback can delegate part or all of it to a savings account watched over by Cyberlife.

## Cashback Markets

Cashback is a well known word, although this industry currently suffers from a multitude of pyramid schemes, closed-door deals and shady practices. Cyberlife chose to introduce blockchain :link: in the equation in order to show how a fully transparent cashback system should look like.

In this example, we use ThunderCore to host our cashback markets contracts. We integrate a normal e-commerce shop where customers checkout and receive a small amount of cashback for each order. Under the hood, the e-commerce shop has its own settings regarding how much cashback it gives to its customers. Each cashback request is serialized and sent to a clearing house smart contract. The clearing house is being watched by a Cyberlife relayer which processes each cashback order and gives the correct amount of money to the customer and a small percentage to a charity. 

We also have an experimental feature: the e-commerce shop chose to create its own "stock" in the form of a bonding curve. Some of the cashback money always goes to the curve, thus incentivizing early customers who were first to buy something from the shop and can now exit the curve with more money in their pockets.

This project addresses ThunderCore's challenge by giving people an alternative to legacy cashback systems and incentivizing them to check out new local businesses where they can get some of that business' crypto stock and also crypto cashback which can be redeemed for fiat.

The post-competition plan for cashback markets is to integrate them with a card where users collect their money and then swap it for fiat or multiply their earnings by using the [banking protocol](https://gitlab.com/cyberlife/cyberlife-js/tree/master/bank-protocol). Also, given that Thunder is so performant, we are thinking about migrating the entire Cyberlife system on this blockchain.
