//MAIN-MARKETS

var CashbackMarkets = artifacts.require("contracts/main/markets/CashbackMarkets.sol");
var ClearingHouse = artifacts.require("contracts/main/markets/ClearingHouse.sol");
var Partenered = artifacts.require("contracts/main/markets/Partenered.sol");
var CashbackLib = artifacts.require("contracts/main/markets/CashbackLib.sol");
var CashbackPreference = artifacts.require("contracts/main/markets/CashbackPreference.sol");
var Treasury = artifacts.require("contracts/main/markets/Treasury.sol");

//MAIN-BONDING

var FundingSources = artifacts.require("contracts/main/bonding/FundingSources.sol");

//TOKEN

var DetailedCBT = artifacts.require("contracts/token/DetailedCBT.sol");

//INTEGRATION

var CashbackIntegration = artifacts.require("contracts/integration/CashbackIntegration.sol");

//SHOP

var ECDSA = artifacts.require("contracts/shop/ECDSA.sol");
var Shop = artifacts.require("contracts/shop/Shop.sol");

//VARS

var thunderData = require('../chainData/thunderTestnet.js');

//MODULE

module.exports = async function (deployer, network, accounts) {

  //SHOP-MARKET params
  var minimumCharityCut = 1;
  var cashbackForCompany = 10;
  var forMarket = 10;

  //SHOP-TOKEN params
  var shopTokenName = "SHOP COIN";
  var shopTokenSymbol = "SHP";
  var shopTokenDecimals = 18;
  var shopTokenScale = "5000000000000000000";
  var shopTokenPool = "500000000000000000";
  var shopTokenReserveRatio = 500000;

  //CYBER-TOKEN params
  var cyberTokenName = "CYBER COIN";
  var cyberTokenSymbol = "CYB";
  var cyberTokenDecimals = 18;
  var cyberTokenScale = "5000000000000000000";
  var cyberTokenPool = "500000000000000000";
  var cyberTokenReserveRatio = 500000;

  //Main address
  var mainAddress = accounts[0]

  if (network == 'development') {

    deployer.deploy(CashbackMarkets).then(function() {
      return deployer.deploy(ClearingHouse, minimumCharityCut).then(function() {
        return deployer.deploy(Partenered).then(function() {
          return deployer.deploy(CashbackLib).then(function() {
            return deployer.deploy(CashbackIntegration).then(function() {
              return deployer.deploy(CashbackPreference).then(function() {
                return deployer.deploy(Treasury).then(function() {
                  return deployer.deploy(FundingSources).then(function() {
                    return deployer.deploy(DetailedCBT, shopTokenName, shopTokenSymbol, shopTokenDecimals, shopTokenScale, shopTokenReserveRatio, FundingSources.address, CashbackMarkets.address, {from: mainAddress, value: shopTokenPool}).then(function() {
                      return deployer.deploy(ECDSA).then(function() {
                        return deployer.link(ECDSA, Shop).then(function() {
                          return deployer.deploy(Shop).then(async function() {

      let cashback = await CashbackMarkets.deployed()
      let house = await ClearingHouse.deployed()
      let cashLib = await CashbackLib.deployed()
      let partners = await Partenered.deployed()
      let cashIntegration = await CashbackIntegration.deployed()
      var cashPreference = await CashbackPreference.deployed()
      var treasury = await Treasury.deployed()
      var fundingSources = await FundingSources.deployed()
      var shopCBT = await DetailedCBT.deployed()
      let eShop = await Shop.deployed()

      //Set values in Clearing House

      await house.setCashbackLib(cashLib.address);
      await house.changeDefaultCharity(accounts[4]);
      await house.setCashbackMarkets(cashback.address);
      await house.setPartners(partners.address);

      //Set values in Cashback Markets

      await cashback.setClearingHouse(house.address);
      await cashback.setCashbackPreferences(cashPreference.address)
      await cashback.setTreasury(treasury.address)
      await cashback.setPartners(partners.address)

      //Set values in Cashback Library

      await cashLib.setHouse(house.address);
      await cashLib.setPartners(partners.address);

      //Set values in Partners contract

      await partners.toggleCompany(cashIntegration.address);

      //Set values in CashbackPreference

      await cashPreference.changePreference(30, {from: mainAddress})
      await cashPreference.changePreference(30, {from: accounts[1]})
      await cashPreference.changePreference(30, {from: accounts[2]})
      await cashPreference.changePreference(30, {from: accounts[3]})
      await cashPreference.changePreference(30, {from: accounts[4]})

      //Set values in FundingSources

      //TODO

      //Set values in DetailedCBT

      var mainAccountShopTknBalance = await shopCBT.balanceOf(mainAddress);

      await shopCBT.transfer(cashIntegration.address, mainAccountShopTknBalance);

      //Set values in Cashback Integration

      await cashIntegration.setHouse(house.address);
      await cashIntegration.setPartners(partners.address);
      await cashIntegration.setPreferences(cashPreference.address);
      await cashIntegration.setLib(cashLib.address);
      await cashIntegration.setMainDapp(eShop.address);
      await cashIntegration.setLogicContract(cashback.address);

      await cashIntegration.initPersonalDetails("Clothing Shop",
        cashbackForCompany, forMarket);

      await cashIntegration.setToken(shopCBT.address);

      await cashIntegration.contributeForFees({from: mainAddress, value: web3.utils.toWei('0.5', 'ether')})

      //Set values in Treasury

      await treasury.setMarkets(cashback.address, {from: mainAddress});
      await treasury.setLoanTreasury(accounts[1], {from: mainAddress});
      await treasury.setSavingsManager(accounts[2], {from: mainAddress});

      await treasury.setInvestmentPreference("savings", 10, {from: mainAddress});
      await treasury.setInvestmentPreference("lending", 10, {from: mainAddress});

      await treasury.setInvestmentPreference("savings", 10, {from: accounts[1]});
      await treasury.setInvestmentPreference("lending", 10, {from: accounts[1]});

      await treasury.setInvestmentPreference("savings", 10, {from: accounts[2]});
      await treasury.setInvestmentPreference("lending", 10, {from: accounts[2]});

      //Set values in Shop

      await eShop.setCashbackIntegration(cashIntegration.address);
      await eShop.setManualCharityFill(false, {from: mainAddress});

      console.log("Local deployment successful!")

    }) }) }) }) }) }) }) }) }) }) }) })

  } else if (network == "thunderTestnet") {

    mainAddress = thunderData.thunderMain;

    deployer.deploy(CashbackMarkets).then(function() {
      return deployer.deploy(FundingSources).then(function() {
        return deployer.deploy(ClearingHouse, minimumCharityCut).then(function() {
          return deployer.deploy(Partenered).then(function() {
            return deployer.deploy(CashbackLib).then(function() {
              return deployer.deploy(CashbackIntegration).then(function() {
                return deployer.deploy(CashbackPreference).then(function() {
                  return deployer.deploy(Treasury).then(function() {
                    return deployer.deploy(DetailedCBT, shopTokenName, shopTokenSymbol, shopTokenDecimals, shopTokenScale, shopTokenReserveRatio, FundingSources.address, CashbackMarkets.address, {from: mainAddress, value: shopTokenPool}).then(function() {
                      return deployer.deploy(ECDSA).then(function() {
                        return deployer.link(ECDSA, Shop).then(function() {
                          return deployer.deploy(Shop).then(async function() {

      let cashback = await CashbackMarkets.deployed()
      let house = await ClearingHouse.deployed()
      let cashLib = await CashbackLib.deployed()
      let partners = await Partenered.deployed()
      let cashIntegration = await CashbackIntegration.deployed()
      var cashPreference = await CashbackPreference.deployed()
      var treasury = await Treasury.deployed()
      var fundingSources = await FundingSources.deployed()
      var shopCBT = await DetailedCBT.deployed()
      let eShop = await Shop.deployed()

      //Set values in Clearing House

      await house.setCashbackLib(cashLib.address);
      await house.changeDefaultCharity(thunderData.otherAccounts[3]);
      await house.setCashbackMarkets(cashback.address);
      await house.setPartners(partners.address);

      //Set values in Cashback Markets

      await cashback.setClearingHouse(house.address);
      await cashback.setCashbackPreferences(cashPreference.address)
      await cashback.setTreasury(treasury.address)
      await cashback.setPartners(partners.address)

      //Set values in Cashback Library

      await cashLib.setHouse(house.address);
      await cashLib.setPartners(partners.address);

      //Set values in Partners contract

      await partners.toggleCompany(cashIntegration.address);

      //Set values in CashbackPreference

      await cashPreference.changePreference(30, {from: mainAddress})
      await cashPreference.setPreferenceFor(thunderData.otherAccounts[0], 30, {from: mainAddress})
      await cashPreference.setPreferenceFor(thunderData.otherAccounts[1], 30, {from: mainAddress})
      await cashPreference.setPreferenceFor(thunderData.otherAccounts[2], 30, {from: mainAddress})
      await cashPreference.setPreferenceFor(thunderData.otherAccounts[3], 30, {from: mainAddress})

      //Set values in FundingSources

      //TODO

      //Set values in DetailedCBT

      var mainAccountShopTknBalance = await shopCBT.balanceOf(mainAddress);

      await shopCBT.transfer(cashIntegration.address, mainAccountShopTknBalance);

      //Set values in Cashback Integration

      await cashIntegration.setHouse(house.address);
      await cashIntegration.setPartners(partners.address);
      await cashIntegration.setPreferences(cashPreference.address);
      await cashIntegration.setLib(cashLib.address);
      await cashIntegration.setMainDapp(eShop.address);
      await cashIntegration.setLogicContract(cashback.address);

      await cashIntegration.initPersonalDetails("Clothing Shop",
        cashbackForCompany, forMarket);

      await cashIntegration.setToken(shopCBT.address);

      await cashIntegration.contributeForFees({from: mainAddress, value: web3.utils.toWei('0.5', 'ether')})

      //Set values in Treasury

      await treasury.setMarkets(cashback.address, {from: mainAddress});
      await treasury.setLoanTreasury(thunderData.otherAccounts[1], {from: mainAddress});
      await treasury.setSavingsManager(thunderData.otherAccounts[2], {from: mainAddress});

      await treasury.setInvestmentPreference("savings", 10, {from: mainAddress});
      await treasury.setInvestmentPreference("lending", 10, {from: mainAddress});

      //Set values in Shop

      await eShop.setCashbackIntegration(cashIntegration.address);
      await eShop.setManualCharityFill(false, {from: mainAddress});

      console.log("Thunder deployment successful!")

    }) }) }) }) }) }) }) }) }) }) }) })

  }

}
