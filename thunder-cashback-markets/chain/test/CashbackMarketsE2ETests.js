//MAIN-MARKETS

var CashbackMarkets = artifacts.require("contracts/main/markets/CashbackMarkets.sol");
var ClearingHouse = artifacts.require("contracts/main/markets/ClearingHouse.sol");
var Partenered = artifacts.require("contracts/main/markets/Partenered.sol");
var CashbackLib = artifacts.require("contracts/main/markets/CashbackLib.sol");
var CashbackPreference = artifacts.require("contracts/main/markets/CashbackPreference.sol");
var Treasury = artifacts.require("contracts/main/markets/Treasury.sol");

//MAIN-BONDING

var FundingSources = artifacts.require("contracts/main/bonding/FundingSources.sol");

//TOKEN

var DetailedCBT = artifacts.require("contracts/token/DetailedCBT.sol");
var CyberCBT = artifacts.require("contracts/token/CyberCBT.sol");

//INTEGRATION

var CashbackIntegration = artifacts.require("contracts/integration/CashbackIntegration.sol");

//SHOP

var ECDSA = artifacts.require("contracts/shop/ECDSA.sol");
var Shop = artifacts.require("contracts/shop/Shop.sol");

var Web3 = require('web3')
var ethers = require('ethers');
const EthCrypto = require('eth-crypto');

var web3

var devPrivateKeys = [

  "0x80113ebd0983a0aebbe9d7e16276bc7f8def180723d78f15c6b392dd191e7466",
  "0xa34c5157521ac1e4eae5be83f95d78da64765a0cabba34f7b917505352de8ee7",
  "0xca6c909c0bead1906a103b29f545c9c6b1cd157ce524d5aae2de1803a99e874d",
  "0x9a83993bd1c34e6a920acd533a493c49fd84f74f4d6ac2a24d6f26eba27659e9",
  "0xcacb122e1ef6107839dc4ed89508bf90b77c43bccea72bec47141e76efd8e420",
  "0xdfae6fd321aced1071d53bbac9944c9dc43197890dc5ed50cfec4cc7f8211a4f"

];

if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
  } else {
    // Set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
}

contract('E2E Cashback Markets Tests', function (accounts) {

    let cashback
    let house
    let cashLib
    let partners
    let cashIntegration
    var cashPreference
    var treasury
    var fundingSources
    var shopCBT
    var ecdsa
    var cyberCBT
    let eShop

    let cashbackMarketsAddress;
    let tokenAddress;
    let bondingVault;

    //SHOP-MARKET params
    var minimumCharityCut = 1;
    var cashbackForCompany = 10;
    var forMarket = 10;

    //SHOP-TOKEN params
    var shopTokenName = "SHOP COIN";
    var shopTokenSymbol = "SHP";
    var shopTokenDecimals = 18;
    var shopTokenScale = "5000000000000000000";
    var shopTokenPool = "500000000000000000";
    var shopTokenReserveRatio = 500000;

    //CYBER-TOKEN params
    var cyberTokenName = "CYBER COIN";
    var cyberTokenSymbol = "CYB";
    var cyberTokenDecimals = 18;
    var cyberTokenScale = "5000000000000000000";
    var cyberTokenPool = "500000000000000000";
    var cyberTokenReserveRatio = 500000;

    let shopOrder = "Ladies+Modern+Stretch+Full+Zip_ladies_outerwear_41.60"

    beforeEach('Setting up', async () => {

      cashback = await CashbackMarkets.new();
      house = await ClearingHouse.new(minimumCharityCut);
      partners = await Partenered.new();
      cashLib = await CashbackLib.new();
      cashIntegration = await CashbackIntegration.new();
      cashPreference = await CashbackPreference.new();
      treasury = await Treasury.new();
      fundingSources = await FundingSources.new();

      shopCBT = await DetailedCBT.new(shopTokenName, shopTokenSymbol, shopTokenDecimals,
                                      shopTokenScale, shopTokenReserveRatio,
                                      fundingSources.address, cashback.address,
                                      {from: accounts[0], value: shopTokenPool});

      cyberCBT = await CyberCBT.new(cyberTokenName, cyberTokenSymbol, cyberTokenDecimals,
                                    cyberTokenScale, cyberTokenReserveRatio,
                                    fundingSources.address, cashback.address,
                                    {from: accounts[0], value: cyberTokenPool});

      ecdsa = await ECDSA.new();

      await Shop.link(ECDSA, ecdsa.address);
      eShop = await Shop.new();

      //Set vars

      //Set values in Clearing House

      await house.setCashbackLib(cashLib.address);
      await house.changeDefaultCharity(accounts[4]);
      await house.setCashbackMarkets(cashback.address);
      await house.setPartners(partners.address);

      //Set values in Cashback Markets

      await cashback.setClearingHouse(house.address);
      await cashback.setCashbackPreferences(cashPreference.address)
      await cashback.setTreasury(treasury.address)
      await cashback.setPartners(partners.address)

      //Set values in Cashback Library

      await cashLib.setHouse(house.address);
      await cashLib.setPartners(partners.address);

      //Set values in Partners contract

      await partners.toggleCompany(cashIntegration.address);

      //Set values in CashbackPreference

      await cashPreference.changePreference(30, {from: accounts[0]})
      await cashPreference.changePreference(30, {from: accounts[1]})
      await cashPreference.changePreference(30, {from: accounts[2]})
      await cashPreference.changePreference(30, {from: accounts[3]})
      await cashPreference.changePreference(30, {from: accounts[4]})

      //Set values in FundingSources

      //TODO

      //Set values in DetailedCBT

      var mainAccountShopTknBalance = await shopCBT.balanceOf(accounts[0]);

      await shopCBT.transfer(cashIntegration.address, mainAccountShopTknBalance);

      //Set values in CyberCBT

      //TODO

      //Set values in Cashback Integration

      await cashIntegration.setHouse(house.address);
      await cashIntegration.setPartners(partners.address);
      await cashIntegration.setLib(cashLib.address);
      await cashIntegration.setPreferences(cashPreference.address);
      await cashIntegration.setMainDapp(eShop.address);
      await cashIntegration.setLogicContract(cashback.address);

      await cashIntegration.initPersonalDetails("Clothing Shop",
        cashbackForCompany, forMarket);

      await cashIntegration.setToken(shopCBT.address);

      await cashIntegration.contributeForFees({from: accounts[0], value: web3.utils.toWei('0.5', 'ether')})

      //Set values in Treasury

      await treasury.setMarkets(cashback.address);

      //Set values in Shop

      await eShop.setCashbackIntegration(cashIntegration.address);
      await eShop.setManualCharityFill(false, {from: accounts[0]});

    });

    it("should check that the shop was added correctly as a partner", async () => {

      assert(await partners.isCompanyMember(cashIntegration.address), true);
      assert(await partners.isInitialized(cashIntegration.address), true);
      assert(await partners.getCompanyName(cashIntegration.address), "Clothing Shop");
      assert(await partners.getCompanyCashbackAmount(cashIntegration.address), 10);
      assert(await partners.getCompanyMarketCut(cashIntegration.address), 10);
      assert(await partners.getBondingToken(cashIntegration.address), shopCBT.address);

    })

    it("should create a serialized order and check it", async () => {

      var orderData;

      var moneyToSend = "1000000000000"

      orderData = await cashLib.
        computeTwoPartyOrder(accounts[1], cashIntegration.address, moneyToSend);

      assert(orderData[1].toString() == "100000000000");
      assert.equal(await cashLib.correctOrder(orderData[0], orderData[1]), true);

    })

    it("should check that an address with no tokens cannot get any cashback", async () => {

      assert(await shopCBT.balanceOf(accounts[1]) == 0, "accounts[1] actually has tokens");
      assert(await shopCBT.balanceOf(accounts[0]) == 0, "accounts[0] actually has tokens");

      var totalCashbackBusiness = await cashback.getTotalReturn(accounts[0]);
      var totalCashbackCustomer = await cashback.getTotalReturn(accounts[1]);

      assert(totalCashbackBusiness == 0, "The business cannot get any cashback");
      assert(totalCashbackCustomer == 0, "The customer cannot get any cashback");

    })

    it("should add a shop order, create a valid cashback order and then execute it", async () => {

      var acc1PublicKey = EthCrypto.publicKeyByPrivateKey(
        devPrivateKeys[0]
      );

      var encryptedMsg = await EthCrypto.encryptWithPublicKey(
        acc1PublicKey,
        shopOrder
      );

      var stringifiedMsg = EthCrypto.cipher.stringify(encryptedMsg);

      var wallet = new ethers.Wallet(devPrivateKeys[0]);
      var signature = await wallet.signMessage(stringifiedMsg)

      // Split the signature into its r, s and v (Solidity's format)
      var sig = await ethers.utils.splitSignature(signature);

      //Buy stuff from the shop and create cashback order

      var moneyToSend = "1000000000000"

      await eShop.buySomething(accounts[2],
                               stringifiedMsg.toString(),
                               sig.v,
                               sig.r,
                               sig.s,
                               {from: accounts[0], value: moneyToSend});

      //Verify order

      var ordersNumber = await house.getOrdersNumber(cashIntegration.address);

      assert(ordersNumber == 1);

      assert(await house.wasOrderExecuted(cashIntegration.address, 0) == false);

      assert(await house.orderHasCharity(cashIntegration.address, 0) == true);

      var orderData = await house.getOrderData(cashIntegration.address, 0)

      var orderParams = await web3.eth.abi.decodeParameters
        (['uint256', 'address[]', 'uint256[]', 'uint256'], orderData)

      assert(orderParams[0] == 1);

      assert(orderParams[1][0] == cashIntegration.address);

      assert(orderParams[1][1] == accounts[2]);

      assert(orderParams[2][0] == 10 && orderParams[2][1] == 89);

      assert(orderParams[3] == 100000000000);

      //Process order

      var fundBalanceBefore = await web3.eth.getBalance(accounts[4])

      assert((await treasury.getTreasuryBalance(cashIntegration.address)).toString() == '0')
      assert((await shopCBT.balanceOf(accounts[2])).toString() == '0')

      await house.processOrder(cashIntegration.address, 0, {from: accounts[3]});

      var fundBalanceAfter = await web3.eth.getBalance(accounts[4]);

      assert(fundBalanceBefore < fundBalanceAfter)

      var customerBalance = await shopCBT.balanceOf(accounts[2])

      assert.equal(customerBalance, 13349999821)

      var businessBalance = await shopCBT.balanceOf(cashIntegration.address)

      assert.equal(businessBalance, '500000000000000000')

      assert(await house.getOrderCharity(cashIntegration.address, 0)
        == accounts[4]);

      assert((await treasury.getTreasuryBalance(cashIntegration.address)).toString() == '10000000000')

      assert((await treasury.getTreasuryBalance(accounts[2])).toString() == '62300000000')

      assert((await cashback.getTotalReturn(accounts[2])).toString() == 26699999998);
      assert((await cashback.getTotalReturn(cashIntegration.address)).toString() == 0)
      assert((await cashback.getReturnFrom(shopCBT.address, accounts[2])).toString() == 26699999998)
      assert((await cashback.returnFromFundAt(accounts[2], 0)).toString() == 26699999998)
      assert((await cashback.returnFromFundAt(accounts[2], 1)).toString() == 0)
      assert((await cashback.getFundsNumber(accounts[2])).toString() == 1)
      assert((await cashback.isInvested(accounts[2], cashIntegration.address)).toString() == 'true')
      assert((await shopCBT.totalSupply()).toString() == '500000013349999821')
      assert((await shopCBT.calculateCurvedBurnReturn('500000000000000000')).toString() == '500000026699999147')

    })

    it("should fail on default fallback", async () => {

        try {

            await web3.eth.sendTransaction({to: cashbackMarketsAddress, from: bob, value: web3.utils.toWei('1', 'wei')})
            assert.ok(false, 'contract must have thrown an error')

        } catch (error) {

            assert.ok(true, 'no fallback ')

        }

    })

})
