pragma solidity 0.5.3;

import "contracts/interfaces/IClearingHouse.sol";
import "contracts/interfaces/IPartenered.sol";
import "contracts/interfaces/ICashbackLib.sol";
import "contracts/interfaces/ICashbackIntegration.sol";
import "contracts/interfaces/ILogic.sol";
import "contracts/interfaces/ICashbackPreference.sol";
import "contracts/zeppelin/ERC20/IBondingERC20.sol";

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/ContractDetector.sol";

contract CashbackIntegration is ICashbackIntegration, Ownable, ContractDetector {

  IClearingHouse house;
  IPartenered partners;
  ICashbackLib cashLib;
  ILogic logic;
  ICashbackPreference preferences;

  address mainDapp;

  uint256 latestFee;

  uint256 fallbackFunds;

  mapping(address => uint256) contributedFunds;

  modifier onlyDapp {

    require(msg.sender == mainDapp, "The sender is not the main dapp");

    _;

  }

  modifier afterInitialization {

    require(address(cashLib) != address(0), "The cashback library was not initialized");
    require(address(partners) != address(0), "The partenered contract was not initialized");
    require(address(house) != address(0), "The clearing house contract was not initialized");
    require(address(preferences) != address(0), "The preferences was not set");

    _;

  }

  constructor() public {

    //TODO: change this fixed fee to a dynamic one; use oracle to get latest fee that guarantees fast order filling

    latestFee = 10 ** 5;

  }

  function setHouse(address _house) public onlyOwner {

    require(isContract(_house) == true, "The address must be from a contract");

    house = IClearingHouse(_house);

    emit SetHouse(_house);

  }

  function setPartners(address _partners) public onlyOwner {

    require(isContract(_partners) == true, "The address must be from a contract");

    partners = IPartenered(_partners);

    emit SetPartners(_partners);

  }

  function setPreferences(address _preferences) public onlyOwner {

    require(isContract(_preferences) == true, "The address must be from a contract");

    preferences = ICashbackPreference(_preferences);

    emit SetPreferences(_preferences);

  }

  function setLib(address _lib) public onlyOwner {

    require(isContract(_lib) == true, "The address must be from a contract");

    cashLib = ICashbackLib(_lib);

    emit SetLib(_lib);

  }

  function setMainDapp(address _dapp) public onlyOwner {

    require(isContract(_dapp) == true, "_dapp is not a contract address");

    mainDapp = _dapp;

    emit SetMainDapp(_dapp);

  }

  function setLogicContract(address payable _logic) public onlyOwner {

    require(isContract(_logic) == true, "_logic is not a contract address");

    logic = ILogic(_logic);

    emit SetLogic(_logic);

  }

  //Partenered

  function initPersonalDetails(string memory _name, uint256 _cashback, uint256 _market)
    public onlyOwner {

    partners.initPersonalDetails(_name, _cashback, _market);

  }

  function changeName(string memory _name) public onlyOwner {

    partners.changeName(_name);

  }

  function changeCompanyCashback(uint256 cashback) public onlyOwner {

    partners.changeCompanyCashback(cashback);

  }

  function changeMarketCut(uint256 market) public onlyOwner {

    partners.changeMarketCut(market);

  }

  function setToken(address payable _token) public onlyOwner {

    require(isContract(_token) == true, "The _token is not a contract address");

    partners.setToken(_token);

  }

  //Cashback Preferences

  function changePreference(uint256 bondedPerc) public onlyOwner {

    preferences.changePreference(bondedPerc);

  }

  //Clearing House

  function submitOrder(bytes memory data,
                       bool manualCharityFill)
                       public payable onlyDapp afterInitialization {

    require(cashLib.correctOrder(data, msg.value) == true, "The order is not correctly formed");
    require(address(this).balance >= latestFee, "The contract does not have enough money to pay the fee");

    house.addMarketOrder.value(msg.value + latestFee)
                         (address(this), data, manualCharityFill, latestFee);

  }

  function setCharityToOrder(uint256 position, address payable _charity) public onlyOwner {

    house.setCharity(position, _charity);

  }

  function() external payable {

    require(msg.value > 0, "You must send a positive amount of money");

    fallbackFunds = fallbackFunds + msg.value;

    emit GotFallbackFunds(msg.sender, msg.value);

  }

  function contributeForFees() public payable {

    require(msg.value > 0, "You must send a positive amount of money");

    contributedFunds[msg.sender] = contributedFunds[msg.sender] + msg.value;

    emit GotFeeFunds(msg.sender, msg.value);

  }

  function extractFallbackFunds(address payable to, uint256 funds) public onlyOwner {

    require(fallbackFunds >= funds, "You cannot extract that much money from fallbackFunds");

    fallbackFunds = fallbackFunds - funds;

    to.transfer(funds);

    emit ExtractedFallbackFunds(msg.sender, to, funds);

  }

  function getBackContribution(uint256 amount) public {

    require(contributedFunds[msg.sender] >= amount, "You cannot withdraw that much money");

    contributedFunds[msg.sender] = contributedFunds[msg.sender] - amount;

    msg.sender.transfer(amount);

    emit GotBackContribution(msg.sender, amount);

  }

  //Business Bonding Curve Token

  function burnBusinessToken(address payable _token, uint256 amount) public onlyOwner {

    logic.sellFromCurve(_token, amount);

  }

  function transferCBTERC20(address payable _token, address to, uint256 amount) public onlyOwner {

    IBondingERC20(_token).transfer(to, amount);

  }

  //GETTERS

  function getTwoPartyOrder(address sender, uint256 money)
    public view returns (bytes memory, uint256) {

    return cashLib.computeTwoPartyOrder(sender, address(this), money);

  }

  function getLatestFee() public view returns (uint256) {

    return latestFee;

  }

}
