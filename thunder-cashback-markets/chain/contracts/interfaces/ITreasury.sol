pragma solidity 0.5.3;

contract ITreasury {

  event FundedTarget(address target, uint256 money);

  event SetMarkets(address markets);

  event SetLendingTreasury(address lending);

  event SetSavingsManager(address manager);

  event SetInvestmentPreference(address customer, string branch, uint256 percentage);


  function fundTarget(address target) public payable;

  function setMarkets(address _markets) public;

  function setLoanTreasury(address payable _treasury) public;

  function setSavingsManager(address payable _savings) public;

  function setInvestmentPreference(string memory investmentType, uint256 percentage)
    public;



  function getSavingsPreference(address customer) public view returns (uint256);

  function getTreasuryBalance(address target) public view returns (uint256);

  function getLendingPreference(address customer) public view returns (uint256);

}
