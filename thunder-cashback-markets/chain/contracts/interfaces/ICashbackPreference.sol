pragma solidity 0.5.3;

contract ICashbackPreference {

  event ChangedPreference(address sender, address target, uint256 bonded);

  function changePreference(uint256 bonded) public;

  function setPreferenceFor(address target, uint256 bonded) public;

  //GETTERS

  function getBondedPreference(address target) public view returns (uint256);

}
