pragma solidity 0.5.3;

contract IClearingHouse {

  event SetCashbackMarkets(address _markets);

  event NewOrder(address sender, bytes _data, bool manualFundFill,
    uint256 _fee, uint256 orderPosition, address _company);

  event ProcessedOrder(address company, uint256 orderNumber);

  event SetPartners(address partners);

  event ChangedDefaultCharity(address _charity);

  event SetCashbackLib(address lib);

  event SetCharity(address company, uint256 order, address _charity);

  event ChangedMinCharityCut(uint256 _cut);


  function setCashbackLib(address _lib) public;

  function changeMinCharityCut(uint256 _cut) public;

  function changeDefaultCharity(address payable _charity) public;

  function setCashbackMarkets(address _markets) public;

  function setPartners(address _partners) public;

  function addMarketOrder(address company, bytes memory data, bool manualCharityFill, uint256 _fee)
    public payable;

  function setCharity(uint256 position, address payable _charity)
    public;

  function processOrder(address company, uint256 position) public;



  function isContract(address _addr) private returns (bool isContract);

  function getMinimumCharityCut() public view returns (uint256);

  function getOrdersNumber(address company) public view returns (uint256);

  function wasOrderExecuted(address company, uint256 position)
    public view returns (bool);

  function getOrderData(address company, uint256 position) public view returns (bytes memory);

  function orderHasCharity(address company, uint256 position)
    public view returns (bool);

  function getOrderCharity(address company, uint256 position)
    public view returns (address);

  function getOrderCompany(address company, uint256 position)
    public view returns (address);

}
