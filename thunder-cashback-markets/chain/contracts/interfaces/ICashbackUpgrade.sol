pragma solidity 0.5.3;

contract ICashbackUpgrade {

  event ChangedUpgradeOwner(address pastOwner, address newOwner);

  event SetCashbackIntegration(address cashback);

  event SetManualCharityFill(bool manual);

  event MovedFunds(uint256 amount, address target);

}
