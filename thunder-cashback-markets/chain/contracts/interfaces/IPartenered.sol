pragma solidity 0.5.3;

contract IPartenered {

  event ToggledCompany(address indexed _company);

  event SetDetails(address indexed _company, string _name, uint256 _cashback, uint256 _market);

  event ChangedName(address indexed company, string newName);

  event ChangedCompanyCashback(address indexed company, uint256 cashback);

  event ChangedMarketCut(address indexed company, uint256 market);

  event SetBondingToken(address company, address token);


  function toggleCompany(address _company) public;

  function initPersonalDetails(string memory _name, uint256 _cashback, uint256 _market)
    public;

  function changeName(string memory _name) public;

  function changeCompanyCashback(uint256 cashback) public;

  function changeMarketCut(uint256 market) public;

  function setToken(address payable _tokenContract) public;

  function isCompanyMember(address _company) public view returns (bool);

  function isInitialized(address company) public view returns (bool);

  function getCompanyName(address _company) public view returns (string memory);

  function getCompanyCashbackAmount(address _company) public view returns (uint256);

  function getCompanyMarketCut(address _company) public view returns (uint256);

  function getBondingToken(address _company) public view returns (address payable);

}
