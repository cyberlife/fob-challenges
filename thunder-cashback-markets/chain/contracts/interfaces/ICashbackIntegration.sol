pragma solidity 0.5.3;

contract ICashbackIntegration {

  event SetHouse(address house);

  event SetPartners(address partners);

  event SetLib(address lib);

  event SetMainDapp(address main);

  event SetLogic(address logic);

  event SetWorkCash(address _tkn);

  event GotFeeFunds(address sender, uint256 money);

  event GotBackContribution(address target, uint256 amount);

  event GotFallbackFunds(address sender, uint256 amount);

  event SetPreferences(address preferences);

  event ExtractedFallbackFunds(address caller, address to, uint256 amount);


  function setHouse(address _house) public;

  function setPartners(address _partners) public;

  function setPreferences(address _preferences) public;

  function setLib(address _lib) public;

  function initPersonalDetails(string memory _name, uint256 _cashback, uint256 _market)
    public;

  function changeName(string memory _name) public;

  function changeCompanyCashback(uint256 cashback) public;

  function changeMarketCut(uint256 market) public;

  function setMainDapp(address _dapp) public;

  function setToken(address payable _token) public;

  function changePreference(uint256 bondedPerc) public;

  function getBackContribution(uint256 amount) public;

  function setLogicContract(address payable _logic) public;

  function burnBusinessToken(address payable _token, uint256 amount) public;

  function transferCBTERC20(address payable _token, address to, uint256 amount) public;

  function contributeForFees() public payable;

  function extractFallbackFunds(address payable to, uint256 funds) public;

  function submitOrder(bytes memory data,
                       bool manualFundFill)
                       public payable;

  function setCharityToOrder(uint256 position, address payable _charity) public;

  function getLatestFee() public view returns (uint256);

  function getTwoPartyOrder(address sender, uint256 money)
    public view returns (bytes memory, uint256);

}
