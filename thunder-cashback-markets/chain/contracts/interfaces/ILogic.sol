pragma solidity 0.5.3;

contract ILogic {

  event SetTreasury(address treasury);

  event SetPartners(address partners);

  event SetClearingHouse(address _house);


  function () external payable;

  function setTreasury(address _treasury) public;

  function setPartners(address _part) public;

  function setClearingHouse(address _house) public;

  function sponsorCurve(address company, address target) public payable;

  function sellFromCurve(address payable token, uint256 _amount) public;

  function getTotalReturn(address target) public view returns (uint256);

  function getReturnFrom(address payable token, address target)
    public view returns (uint256);

  function returnFromFundAt(address target, uint256 position)
    public view returns (uint256);

  function getFundsNumber(address target) public view returns (uint256);

  function isInvested(address target, address company) public view returns (bool);

}
