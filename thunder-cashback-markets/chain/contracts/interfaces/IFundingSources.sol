pragma solidity 0.5.3;

contract IFundingSources {

  event ToggledSource(address token, address source);

  function toggleSource(address token, address source) public;

  function isSourceAvailable(address _tkn, address _source) public view returns (bool);

}
