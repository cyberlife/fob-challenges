pragma solidity 0.5.3;

import "contracts/main/bonding/SimpleCBT.sol";

import "contracts/interfaces/IFundingSources.sol";

contract DetailedCBT is SimpleCBT {

  event ChangedSources(address newSources);
  event FundedWithoutMinting(address source, uint256 amount);

  string private _name;
  string private _symbol;
  uint8 private _decimals;

  IFundingSources sources;

  modifier onlyLegitSource() {

    require(sources.isSourceAvailable(address(this), msg.sender) == true,
      "The sender is not authorized to fund the token without minting coins");

    _;

  }

    constructor(
        string memory name,
        string memory symbol,
        uint8 decimals,
        uint256 _scale,
        uint256 reserveRatio,
        address _sources,
        address _markets
    )   public payable
        SimpleCBT(_scale, msg.value, reserveRatio, _markets)
    {

      _name = name;
      _symbol = symbol;
      _decimals = decimals;

      sources = IFundingSources(_sources);

    }

    function changeSources(address _sources) public onlyOwner {

      sources = IFundingSources(_sources);

      emit ChangedSources(_sources);

    }

    function fundWithoutMinting() public payable onlyLegitSource {

      emit FundedWithoutMinting(msg.sender, msg.value);

    }

    /**
     * @return the name of the token.
     */
    function name() public view returns (string memory) {
        return _name;
    }

    /**
     * @return the symbol of the token.
     */
    function symbol() public view returns (string memory) {
        return _symbol;
    }

    /**
     * @return the number of decimals of the token.
     */
    function decimals() public view returns (uint8) {
        return _decimals;
    }

}
