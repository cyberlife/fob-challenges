pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/SafeMath.sol";
import "contracts/zeppelin/ContractDetector.sol";

import "contracts/interfaces/ITreasury.sol";

contract Treasury is Ownable, ITreasury, ContractDetector {

  using SafeMath for uint256;

  address cashbackMarkets;

  address payable savingsManager;
  address payable loanTreasury;

  mapping(address => uint256) ethBalances;

  mapping(address => mapping(address => uint256)) investmentPreferences;

  modifier onlyMarkets() {

    require(msg.sender == cashbackMarkets, "The sender is not the markets contract");
    _;

  }

  constructor() public {}

  function() external payable {

    revert();

  }

  //OWNER

  function setMarkets(address _markets) public onlyOwner {

    require(isContract(_markets) == true, "The param specified is not a contract address");
    cashbackMarkets = _markets;

    emit SetMarkets(_markets);

  }

  function setLoanTreasury(address payable _treasury) public onlyOwner {

    loanTreasury = _treasury;

    emit SetLendingTreasury(_treasury);

  }

  function setSavingsManager(address payable _savings) public onlyOwner {

    savingsManager = _savings;

    emit SetSavingsManager(_savings);

  }

  function setInvestmentPreference(string memory investmentType, uint256 percentage) public {

    require(keccak256(abi.encode(investmentType)) == keccak256(abi.encode("savings")) ||
            keccak256(abi.encode(investmentType)) == keccak256(abi.encode("lending")),
            "No valid investment type specified");

    if (keccak256(abi.encode(investmentType)) == keccak256(abi.encode("savings"))) {

      require(percentage + investmentPreferences[msg.sender][loanTreasury] <= 100,
        "Cannot specify more than 100% for lending + savings preferences");

      investmentPreferences[msg.sender][savingsManager] = percentage;

    } else if (keccak256(abi.encode(investmentType)) == keccak256(abi.encode("lending"))) {

      require(percentage + investmentPreferences[msg.sender][savingsManager] <= 100,
        "Cannot specify more than 100% for lending + savings preferences");

      investmentPreferences[msg.sender][loanTreasury] = percentage;

    }

    emit SetInvestmentPreference(msg.sender, investmentType, percentage);

  }

  function fundTarget(address target) public payable onlyMarkets() {

    uint256 remainingCashback =
      100 -
      (investmentPreferences[target][loanTreasury]
        + investmentPreferences[target][savingsManager]);

    uint256 forLending = msg.value * investmentPreferences[target][loanTreasury] / 100;

    uint256 forSavings = 0;

    if (remainingCashback == 0) {

      forSavings = msg.value - forLending;

    } else {

      forSavings = msg.value * investmentPreferences[target][savingsManager] / 100;

    }

    if (forLending > 0) {

      loanTreasury.transfer(forLending);

    }

    if (forSavings > 0) {

      savingsManager.transfer(forSavings);

    }

    if (remainingCashback < 100)
      ethBalances[target] = ethBalances[target].add(msg.value - (forSavings + forLending));

    emit FundedTarget(target, msg.value);

  }

  //GETTERS

  function getTreasuryBalance(address target) public view returns (uint256) {

    return ethBalances[target];

  }

  function getSavingsPreference(address customer) public view returns (uint256) {

    return investmentPreferences[customer][savingsManager];

  }

  function getLendingPreference(address customer) public view returns (uint256) {

    return investmentPreferences[customer][loanTreasury];

  }

}
