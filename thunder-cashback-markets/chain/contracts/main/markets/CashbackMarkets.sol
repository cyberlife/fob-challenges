pragma solidity 0.5.3;

import "contracts/main/markets/Logic.sol";
import "contracts/interfaces/ICashbackMarkets.sol";
import "contracts/interfaces/ICashbackPreference.sol";

/**
 * @title Proof of Concept contract for Cashback Markets
 */

contract CashbackMarkets is ICashbackMarkets, Logic {

    mapping(address => bool) availableCharities;

    string charitiesListIPFS;

    ICashbackPreference preferences;

    constructor() Logic() public {}

    /**
    * @dev The sender can split the money between a donation and cashback
    */
    function executeOrder(address company, address payable targetCharity,
                          uint256 charityPercentage,
                          address[] memory targets, uint256[] memory paymentShares)
                          public payable onlyClearingHouse {

        require(company != address(0), "The company must be specified");
        require(charityPercentage <= 100 && charityPercentage >= 1, "The charity percentage is not between 1 and 100 percent");
        require(targetCharity != address(0), "Charity address is not set correctly");
        require(msg.value > 0, "Must include some money to donate");

        // Make money distributions
        uint256 multiplier = 100;
        uint256 charityAllocation = (msg.value).mul(charityPercentage).div(multiplier);
        uint256 cashbackAllocation = msg.value - charityAllocation;

        uint256 alreadyAllocatedFunds = 0;
        uint256 currentEthAllocation = 0;

        sendToCharity(targetCharity, charityAllocation.div(multiplier));

        if (paymentShares.length > 0) {

          distributePayments(

            msg.value,
            cashbackAllocation,
            company,
            targets,
            paymentShares

          );

        } else {

          //If no targets, reward the charity by minting bonding cashback tokens

          this.sponsorCurve.value(cashbackAllocation)
                      (company, targetCharity);

        }

        emit ExecutedOrder(company);

    }

    function distributePayments(uint256 totalPayment,
                                uint256 cashbackAllocation,
                                address company,
                                address[] memory targets,
                                uint256[] memory paymentShares)
                                internal {

      uint256 alreadyAllocatedFunds = 0;
      uint256 currentEthAllocation = 0;

      uint256 bondedPerc = 0;
      uint256 bondedMoney = 0;

      for (uint i = 0; i < paymentShares.length; i++) {

          if (i == paymentShares.length - 1) {

            currentEthAllocation = cashbackAllocation - alreadyAllocatedFunds;

          } else {

            currentEthAllocation = totalPayment * paymentShares[i] / 100;

          }

          bondedPerc = preferences.getBondedPreference(targets[i]);
          bondedMoney = currentEthAllocation * bondedPerc / 100;

          if (bondedMoney > 0) {

            this.sponsorCurve.value(bondedMoney)
                        (company, targets[i]);

          }

          treasury.fundTarget.value(currentEthAllocation - bondedMoney)
                                   (targets[i]);

          alreadyAllocatedFunds = alreadyAllocatedFunds + currentEthAllocation;

      }

    }

    function sendToCharity(address payable charity, uint256 _amount) internal {

      charity.transfer(_amount);

      emit SendToCharity(charity, _amount);

    }

    function setCashbackPreferences(address _preferences)
      public onlyOwner onlyContractParam(_preferences) {

      preferences = ICashbackPreference(_preferences);

      emit SetPreferences(_preferences);

    }

    /**
    * @dev Toggle the status of a '_charityAddress'
    */
    function toggleCharityAddress(string memory ipfsCharities, address payable _charityAddress)
      public onlyOwner {

      require(_charityAddress != address(0), "The specified charity address must not be address(0)");

      availableCharities[_charityAddress] = !availableCharities[_charityAddress];

      charitiesListIPFS = ipfsCharities;

      emit ToggleCharity(ipfsCharities, _charityAddress);

    }

    //GETTERS

    function isCharityAvailable(address _charity) public view returns (bool) {

      return availableCharities[_charity];

    }

}
