pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";

import "contracts/interfaces/IPartenered.sol";

import "contracts/zeppelin/ContractDetector.sol";

contract Partenered is IPartenered, Ownable, ContractDetector {

  struct Company {

    string name;

    uint256 companyCashback;

    uint256 marketCut;

    address payable bondingToken;

  }

  mapping(address => bool) memberCompany;

  mapping(address => Company) companiesDetails;

  modifier onlyMemberCompany() {

    require(memberCompany[msg.sender] == true, "Only a member company can call this");
    _;

  }

  modifier onlyInitializedCompany {

    require(companiesDetails[msg.sender].companyCashback > 0 ||
            companiesDetails[msg.sender].marketCut > 0, "The company sending the tx is not initialized");

    _;

  }

  constructor() public {}

  function toggleCompany(address _company)
    public onlyOwner {

    require(super.isContract(_company) == true, "The company specified is not a contract");

    memberCompany[_company] = !memberCompany[_company];

    emit ToggledCompany(_company);

  }

  function initPersonalDetails(string memory _name, uint256 _cashback, uint256 _market)
    public onlyMemberCompany {

    require(memberCompany[msg.sender] == true, "This company is not a member");

    require(companiesDetails[msg.sender].companyCashback == 0 &&
            companiesDetails[msg.sender].marketCut == 0,
            "The company was already initialized");

    require(_market > 0, "_market needs to be greater than zero");

    Company memory newCompany = Company(_name, _cashback, _market, address(0));

    companiesDetails[msg.sender] = newCompany;

    emit SetDetails(msg.sender, _name, _cashback, _market);

  }

  function setToken(address payable _tokenContract)
    public onlyMemberCompany onlyInitializedCompany {

    require(companiesDetails[msg.sender].bondingToken == address(0),
      "The token for this company was already set");

    companiesDetails[msg.sender].bondingToken = _tokenContract;

    emit SetBondingToken(msg.sender, _tokenContract);

  }

  function changeName(string memory _name)
    public onlyMemberCompany onlyInitializedCompany {

    companiesDetails[msg.sender].name = _name;

    emit ChangedName(msg.sender, _name);

  }

  function changeCompanyCashback(uint256 cashback)
    public onlyMemberCompany onlyInitializedCompany {

    companiesDetails[msg.sender].companyCashback = cashback;

    emit ChangedCompanyCashback(msg.sender, cashback);

  }

  function changeMarketCut(uint256 market)
    public onlyMemberCompany onlyInitializedCompany {

    companiesDetails[msg.sender].marketCut = market;

    emit ChangedMarketCut(msg.sender, market);

  }

  //GETTERS

  function isCompanyMember(address _company) public view returns (bool) {

    return memberCompany[_company];

  }

  function isInitialized(address _company) public view returns (bool) {

    return (companiesDetails[_company].companyCashback > 0 ||
            companiesDetails[_company].marketCut > 0);

  }

  function getCompanyName(address _company) public view returns (string memory) {

    return companiesDetails[_company].name;

  }

  function getCompanyCashbackAmount(address _company) public view returns (uint256) {

    return companiesDetails[_company].companyCashback;

  }

  function getBondingToken(address _company) public view returns (address payable) {

    return companiesDetails[_company].bondingToken;

  }

  function getCompanyMarketCut(address _company) public view returns (uint256) {

    return companiesDetails[_company].marketCut;

  }

}
