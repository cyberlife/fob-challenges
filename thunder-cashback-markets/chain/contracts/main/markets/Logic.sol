pragma solidity 0.5.3;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/SafeMath.sol";
import "contracts/zeppelin/ContractDetector.sol";

import "contracts/token/DetailedCBT.sol";
import "contracts/interfaces/IPartenered.sol";

import "contracts/interfaces/ILogic.sol";
import "contracts/interfaces/ITreasury.sol";

contract Logic is ILogic, Ownable, ContractDetector {

    using SafeMath for uint256;

    address clearingHouse;

    ITreasury treasury;
    IPartenered partners;

    mapping(address => mapping(address => bool)) isAddressInvested;
    mapping(address => mapping(address => bool)) everInvestedIn;
    mapping(address => address[]) fundsPerAddress;

    modifier onlyClearingHouse() {

      require(clearingHouse == msg.sender, "Only the clearing house can call");
      _;

    }

    modifier onlyContractParam(address _toCheck) {

      require(isContract(_toCheck) == true, "The param is not a contract");
      _;

    }

    /**
     * @dev No fallback is allowed, use sponsor() to fund the Bonding Curve
     */
    function () external payable {
        revert();
    }

    function setTreasury(address _treasury) public onlyOwner onlyContractParam(_treasury) {

      treasury = ITreasury(_treasury);

      emit SetTreasury(_treasury);

    }

    function setPartners(address _part) public onlyOwner onlyContractParam(_part) {

      partners = IPartenered(_part);

      emit SetPartners(_part);

    }

    function setClearingHouse(address _house) public onlyOwner onlyContractParam(_house) {

      clearingHouse = _house;

      emit SetClearingHouse(_house);

    }

    function sponsorCurve(address company, address target) public payable //onlyClearingHouse
    {

      require(msg.value > 0, "You need to send money in order to sponsor the curve");

      address payable bondingToken = address(

        uint160(partners.getBondingToken(company))

      );

      require(bondingToken != address(0), "This company does not have a bonding token set");

      DetailedCBT(bondingToken).mint.value(msg.value)(target);

      if (!everInvestedIn[target][company])
        fundsPerAddress[target].push(company);

      everInvestedIn[target][company] = true;

      isAddressInvested[target][company] = true;

    }

    function sellFromCurve(address payable token, uint256 _amount) public onlyContractParam(token) {

      uint256 balanceInToken = DetailedCBT(token).balanceOf(msg.sender);

      if (_amount == balanceInToken) isAddressInvested[msg.sender][token] = false;

      DetailedCBT(token).burn(msg.sender, _amount);

    }

    //GETTERS

    //TODO: need to optimize with a combination of off-chain computation and IPFS

    function getTotalReturn(address target) public view returns (uint256) {

      if (fundsPerAddress[target].length == 0) return 0;

      uint256 totalReturn = 0;
      uint256 positionBalance = 0;
      DetailedCBT cbt;
      address payable tkn;

      for (uint i = 0; i < fundsPerAddress[target].length; i++) {

        if (isAddressInvested[target][fundsPerAddress[target][i]]) {

          tkn = address(

            uint160(partners.getBondingToken(fundsPerAddress[target][i]))

          );

          cbt = DetailedCBT(tkn);

          positionBalance = cbt.balanceOf(target);

          if (positionBalance > 0)
            totalReturn = totalReturn + (cbt.calculateCurvedBurnReturn(positionBalance));

        }

      }

      return totalReturn;

    }

    function getReturnFrom(address payable token, address target)
      public view returns (uint256) {

      uint256 totalBalance = DetailedCBT(token).balanceOf(target);

      return DetailedCBT(token).calculateCurvedBurnReturn(totalBalance);

    }

    function returnFromFundAt(address target, uint256 position)
      public view returns (uint256) {

      if (position >= fundsPerAddress[target].length) return 0;

      address payable tkn = address(

        uint160(partners.getBondingToken(fundsPerAddress[target][position]))

      );

      DetailedCBT cbt = DetailedCBT(tkn);

      uint256 totalBalance = cbt.balanceOf(target);

      return cbt.calculateCurvedBurnReturn(totalBalance);

    }

    function getFundsNumber(address target) public view returns (uint256) {

      return fundsPerAddress[target].length;

    }

    function isInvested(address target, address company) public view returns (bool) {

      return isAddressInvested[target][company];

    }

}
