pragma solidity 0.5.3;

import "contracts/interfaces/ICashbackMarkets.sol";
import "contracts/interfaces/IClearingHouse.sol";
import "contracts/interfaces/IPartenered.sol";
import "contracts/interfaces/ICashbackLib.sol";

import "contracts/zeppelin/ownership/Ownable.sol";

contract ClearingHouse is IClearingHouse, Ownable {

  struct MarketOrder {

    uint256 _nonce;

    address company;

    address payable charity;

    bool manualFundFill;

    bytes orderData;

    uint256 fee;

    bool executed;

  }

  ICashbackMarkets cashbackMarkets;
  IPartenered partners;
  ICashbackLib cashbackLib;

  mapping(address => MarketOrder[]) private marketOrders;
  mapping(bytes => bool) private orderHashes;

  address payable defaultCharity;

  uint256 private minCharityPercentage;

  uint256 nonce;

  modifier afterSetup() {

    require(address(cashbackMarkets) != address(0) &&
            address(partners) != address(0),
            "The setup was not finished");

    _;

  }

  modifier onlyMemberCompany() {

    require(partners.isCompanyMember(msg.sender) == true, "The caller is not a member");

    _;

  }

  constructor(uint256 minCharity) public {

    require(minCharity >= 1, "The charity cut has to be at least 1%");

    minCharityPercentage = minCharity;

    nonce = 0;

  }

  function setCashbackLib(address _lib) public onlyOwner {

    require(isContract(_lib) == true, "The address is not a contract");

    cashbackLib = ICashbackLib(_lib);

    emit SetCashbackLib(_lib);

  }

  function changeMinCharityCut(uint256 _cut) public onlyOwner {

    minCharityPercentage = _cut;

    emit ChangedMinCharityCut(_cut);

  }

  function changeDefaultCharity(address payable _charity) public onlyOwner {

    require(_charity != address(0), "Default must not be null");

    defaultCharity = _charity;

    emit ChangedDefaultCharity(_charity);

  }

  function setCashbackMarkets(address _markets) public onlyOwner {

    require(isContract(_markets) == true, "The param specified is not a cashback markets address");

    cashbackMarkets = ICashbackMarkets(_markets);

    emit SetCashbackMarkets(_markets);

  }

  function setPartners(address _partners) public onlyOwner {

    require(isContract(_partners) == true, "The address must be from a contract");

    partners = IPartenered(_partners);

    emit SetPartners(_partners);

  }

  function addMarketOrder(address _company, bytes memory data,
                          bool manualCharityFill, uint256 _fee)
                          public payable onlyMemberCompany afterSetup {

    address payable charityToFill = address(0);

    if (manualCharityFill == false)
      charityToFill = defaultCharity;

    nonce++;

    MarketOrder memory newOrder
      = MarketOrder(nonce - 1, _company, charityToFill, manualCharityFill,
                    data, _fee, false);

    marketOrders[msg.sender].push(newOrder);

    emit NewOrder(msg.sender, data, manualCharityFill,
                  _fee, marketOrders[msg.sender].length - 1, _company);

  }

  function setCharity(uint256 position, address payable _charity)
    public onlyMemberCompany afterSetup {

    require(position < marketOrders[msg.sender].length,
      "The position needs to be between bounds");

    require(marketOrders[msg.sender][position].charity == address(0),
      "The charity was already specified");

    require(marketOrders[msg.sender][position].executed == false,
        "This market order has already been executed");

    require(cashbackMarkets.isCharityAvailable(_charity) == true,
      "This charity is not available");

    marketOrders[msg.sender][position].charity = _charity;

    emit SetCharity(msg.sender, position, _charity);

  }

  function processOrder(address company, uint256 position) public afterSetup {

    require(position < marketOrders[company].length,
        "The order position needs to be less than marketOrders[company].length");

    require(marketOrders[company][position].charity != address(0), "The fund was not specified");

    require(marketOrders[company][position].executed == false,
        "This market order has already been executed");

    marketOrders[company][position].executed = true;

    uint256 _charityCut;
    address[] memory receivers;
    uint256[] memory shares;
    uint256 money;

    (_charityCut, receivers, shares, money)
        = abi.decode(marketOrders[company][position].orderData,
        (uint256, address[], uint256[], uint256));

    cashbackMarkets.executeOrder.value(money)
                                      (marketOrders[company][position].company,
                                      marketOrders[company][position].charity,
                                      _charityCut,
                                      receivers,
                                      shares);

    msg.sender.transfer(marketOrders[company][position].fee);

    emit ProcessedOrder(company, position);

  }

  //PRIVATE

  function isContract(address _addr) private returns (bool isContract) {

    uint32 size;

    assembly {

      size := extcodesize(_addr)

    }

    return (size > 0);

  }

  //GETTERS

  function getMinimumCharityCut() public view returns (uint256) {

    return minCharityPercentage;

  }

  function getOrdersNumber(address company) public view returns (uint256) {

    return marketOrders[company].length;

  }

  function wasOrderExecuted(address company, uint256 position)
    public view returns (bool) {

    if (position >= marketOrders[company].length) {

      return false;

    }

    return marketOrders[company][position].executed;

  }

  function getOrderData(address company, uint256 position) public view returns (bytes memory) {

    if (position >= marketOrders[company].length) {

      return bytes("");

    }

    return marketOrders[company][position].orderData;

  }

  function orderHasCharity(address company, uint256 position)
    public view returns (bool) {

    return (marketOrders[company][position].charity != address(0));

  }

  function getOrderCharity(address company, uint256 position)
    public view returns (address) {

    return marketOrders[company][position].charity;

  }

  function getOrderCompany(address company, uint256 position) public view returns (address) {

    return marketOrders[company][position].company;

  }

}
