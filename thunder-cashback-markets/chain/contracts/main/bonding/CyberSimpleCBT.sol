pragma solidity 0.5.3;

import "contracts/main/bonding/CyberBondedToken.sol";

contract CyberSimpleCBT is CyberBondedToken {

    event SetMarkets(address markets);

    address cashbackMarkets;

    modifier onlyCashbackMarkets() {

      require(cashbackMarkets == msg.sender, "The sender is not the cashback markets contract");

      _;

    }

    function () external payable { revert(); }

    constructor(uint256 _scale,
                uint256 _poolBalance,
                uint256 _reserveRatio,
                address _cashbackMarkets) public
        CyberBondedToken(_scale, _poolBalance, _reserveRatio)
    {

      require(isContract(_cashbackMarkets) == true,
        "The cashback markets address is not from a contract");

      cashbackMarkets = _cashbackMarkets;

    }

    function setMarkets(address _markets) public onlyOwner {

      require(super.isContract(_markets) == true, "The address is not a contract");

      cashbackMarkets = _markets;

      emit SetMarkets(_markets);

    }

    function mint(address target) public payable onlyCashbackMarkets {
        require(msg.value > 0, "Must send ether to buy tokens.");
        super._curvedMint(target, msg.value);
    }

    function burn(address payable target, uint256 _amount) public onlyCashbackMarkets {
        uint256 returnAmount = _curvedBurn(target, _amount);
        target.transfer(returnAmount);
    }

}
