var HDWalletProvider = require("truffle-hdwallet-provider");

var thunderData = require('./chainData/thunderTestnet.js')

module.exports = {

  networks: {

    development: {
      host: "localhost",
      port: 8545,
      network_id: "*",
      from: "0x6259ac218eed8caf47e26246d7e13c1df70165f2",
      gas: 7590000
    },

    thunderTestnet: {
      provider: function() {
        return new HDWalletProvider(thunderData.thunderMnemonic.toString(),
            "https://testnet-rpc.thundercore.com:8544")
      },
      network_id: "18",
      from: "0x5581364f1350B82Ed4E25874f3727395BF6Ce490",
      gas: 9500000,
      gasPrice: 60000000000
    },

    ropsten: {

      provider: function() {
        return new HDWalletProvider(mnemonic, "https://ropsten.infura.io/")
      },
      network_id: 3,
      gas: 7000000

    },

    rinkeby: {
        provider: function() {
          return new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/")
        },
        network_id: 4,
        gas: 7000000
    }

  },

  mocha: {
    enableTimeouts: false
  },

  compilers: {
    solc: {
      version: "0.5.3",
    },
  },

  solc: {
    optimizer: { // Turning on compiler optimization that removes some local variables during compilation
      enabled: true,
      runs: 200
    }
  }

};
