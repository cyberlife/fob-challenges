Deploying 'CashbackMarkets'
---------------------------
> transaction hash:    0x11ecaf724a14700e4537c9fa99d326b19f7500ec7d05fb8013b2cef4b0b1d53f
> Blocks: 0            Seconds: 0
> contract address:    0x6DB96F47d1B277Cf89e66208466703EFa3c202Df
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             105.888198269993552286
> gas used:            2083869
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.12503214 ETH


Deploying 'FundingSources'
--------------------------
> transaction hash:    0xed222eb621aed792dc5f448d7ded0bc85f24229bcd7dd602801d4ee7f8f11cab
> Blocks: 0            Seconds: 0
> contract address:    0xadBd9108ba27B9b7Bb6Be2330fBe8566668745CA
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             105.867363449993552286
> gas used:            347247
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.02083482 ETH


Deploying 'ClearingHouse'
-------------------------
> transaction hash:    0x6c162873de4e5cacff1b60a144ade3dfe06069e5f915cdcf0e94e108b6113b1a
> Blocks: 0            Seconds: 0
> contract address:    0x33e291795C0C239D922Cf652914392c2AfFf47Cf
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             105.743801009993552286
> gas used:            2059374
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.12356244 ETH


Deploying 'Partenered'
----------------------
> transaction hash:    0x929f8b5ca732096eed70c88cbd4a8e574ba7301abef58f447ec7df9463da3f4b
> Blocks: 0            Seconds: 0
> contract address:    0xaF7b083a7c57666EdC393eAf2F7b74e7e9d4f6F6
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             105.665086289993552286
> gas used:            1311912
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.07871472 ETH


Deploying 'CashbackLib'
-----------------------
> transaction hash:    0xd87ae380bd30ee0aea5fe285724eef3d9db891df3efcdb91cd56ac14623d21e2
> Blocks: 0            Seconds: 0
> contract address:    0xbC51e3b9f4B378d4Ccb6Ec5C5D03598251365031
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             105.601534409993552286
> gas used:            1059198
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.06355188 ETH


Deploying 'CashbackIntegration'
-------------------------------
> transaction hash:    0x81565caae6310974eaca4d4c4d5e3b3acdd2429d8e4d6e1e14317c0c5b098f12
> Blocks: 0            Seconds: 0
> contract address:    0x0Caa184c25845ae90Bddf9B08314069F474dE095
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             105.475619429993552286
> gas used:            2098583
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.12591498 ETH


Deploying 'CashbackPreference'
------------------------------
> transaction hash:    0xa3a181ee5c682ba7320cca3261db7b07a599a1cc2281052e86b730272cbec10e
> Blocks: 5            Seconds: 4
> contract address:    0x952B549a9f690Cf5Ae77Ca8675Ca5Df359581a10
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             105.453077129993552286
> gas used:            375705
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.0225423 ETH


Deploying 'Treasury'
--------------------
> transaction hash:    0xe034b71d8bd7ef2c259615fe4ef6dd2e560bb8c3475c874a1903caac5320b09d
> Blocks: 0            Seconds: 0
> contract address:    0xa1b05A5886B13572a77C0e57ceDd6dBf030D6bAD
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             105.384885089993552286
> gas used:            1136534
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.06819204 ETH


Deploying 'DetailedCBT'
-----------------------
> transaction hash:    0x6d8d450ad69d281c6618fd81cfa7572c97b3aae0f74a50ad52534210e1fe909f
> Blocks: 0            Seconds: 0
> contract address:    0xCC73FD3117f7705B41428e7c70d6d3Fe7B17A213
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             104.574337869993552286
> gas used:            5175787
> gas price:           60 gwei
> value sent:          0.5 ETH
> total cost:          0.81054722 ETH


Deploying 'ECDSA'
-----------------
> transaction hash:    0x29102260c8cf252ab51defdac0f36bdedec84586593cbcc0abc78d9befb77b14
> Blocks: 0            Seconds: 0
> contract address:    0xa7c8c8Ad5Cf1EE67f135195Ae1218962897A4A7e
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             104.557564209993552286
> gas used:            279561
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.01677366 ETH


Linking
-------
* Contract: Shop <--> Library: ECDSA (at address: 0xa7c8c8Ad5Cf1EE67f135195Ae1218962897A4A7e)

Deploying 'Shop'
----------------
> transaction hash:    0x3846a99ab5239c254d6d6f883c56ae107ae8be64838d5987027ba3fa2d55876c
> Blocks: 0            Seconds: 0
> contract address:    0x56AF28b0c53bc12f9624aBb6dF30C1a061B02AC6
> account:             0x5581364f1350B82Ed4E25874f3727395BF6Ce490
> balance:             104.495612949993552286
> gas used:            1032521
> gas price:           60 gwei
> value sent:          0 ETH
> total cost:          0.06195126 ETH
