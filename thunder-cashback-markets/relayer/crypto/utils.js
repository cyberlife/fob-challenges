var houseABI = require('../abi/ClearingHouse.js');

var contractsAddresses = require('./contractsAddresses.js');
var accounts = require('./accounts.js');
var contractCaller = require('./contractCaller.js');

async function getContractInstance(network, providerType, contractName) {

  var web3, houseInstance;

  if (network == "local") {

    web3 = await contractCaller.getWeb3('local', providerType);

    if (contractName == "house") {

      try {

          houseInstance = await new web3.eth.Contract(

            houseABI.house.abi,
            contractsAddresses.localContracts[0],

            {from: accounts.local[0]}

        );

        return houseInstance;

      } catch(err) {

        return undefined;

      }

    }

    return undefined;

  } else if (network == "thunder") {

    web3 = await contractCaller.getWeb3('thunder', providerType);

    if (contractName == "house") {

      try {

          houseInstance = await new web3.eth.Contract(

            houseABI.house.abi,
            contractsAddresses.thunderContracts[0],

            {from: accounts.thunder[0]}

        );

        return houseInstance;

      } catch(err) {

        return undefined;

      }

    }

    return undefined;

  }

}

module.exports = {

  getContractInstance

}
