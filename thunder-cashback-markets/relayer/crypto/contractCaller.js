var Web3 = require('web3');

var accounts = require('./accounts.js');

//65 Gwei
var DEFAULT_GAS_PRICE = 65000000000
var DEFAULT_GAS_LIMIT = 2500000

async function getWeb3(network, type) {

  if (type == undefined) return undefined;

  if (type == "http") {

    if (network == "local")
      return new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

    else if (network == "thunder")
      return new Web3(new Web3.providers.HttpProvider("https://testnet-rpc.thundercore.com:8544"));

    else if (network == "rinkeby") {

      return new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

    } else if (network == "ropsten") {

      return new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/OPVe8KJGtYczgs2np3R2"));

    }

  } else {

    if (network == "local")
      return new Web3(new Web3.providers.WebsocketProvider("http://localhost:8545"));

    else if (network == "thunder")
      return new Web3(new Web3.providers.WebsocketProvider("https://testnet-rpc.thundercore.com:8544"));

    else if (network == "rinkeby") {

      return new Web3(new Web3.providers.WebsocketProvider("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

    } else if (network == "ropsten") {

      return new Web3(new Web3.providers.WebsocketProvider("https://ropsten.infura.io/OPVe8KJGtYczgs2np3R2"));

    }

  }
}

//CASHBACK MARKETS RELAYER

async function processOrder(network, instance, pubKey, privateKey, company, position) {

  if (network == "local") {

    instance.methods.processOrder(company, position).send({

      from: pubKey,
      gas: DEFAULT_GAS_LIMIT

    })
    .on('receipt', (receipt) => {

      console.log("Processed order number " + position.toString() + " from " + company)

    })
    .on('error', (error) => {

      console.log("Could not process order number " + position.toString() + " from " + company)

    })

  } else {

    //Submit tx on Thunder

    var web3 = await getWeb3(network, "http");

    var nonce = await web3.eth.getTransactionCount(pubKey);

    const txParams = {
      "from": pubKey,
      "to": instance.address,
      "data": instance.methods.processOrder(company, position).encodeABI(),
      "value": "0x0",
      "gas": DEFAULT_GAS_LIMIT,
      "nonce": nonce,
      "chainId": 18,
      "gasPrice": DEFAULT_GAS_PRICE
    };

    try {

      web3.eth.accounts.signTransaction(txParams, '0x' + privateKey).then(signed => {

        web3.eth
        .sendSignedTransaction(signed.rawTransaction)
        .on('receipt', (receipt) => {

          console.log("Processed order number " + position.toString() + " from " + company);

        })
        .on('error', (error) => {

          console.log("Could not process order number " + position.toString() + " from " + company);

        })

      })

    } catch(err) {}

  }

}

module.exports = {

  getWeb3,

  processOrder

}
