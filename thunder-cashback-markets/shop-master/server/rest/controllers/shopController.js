var gateway = require('../crypto/chain/chainGateway.js');

exports.shop_post_order = async (req, res, next) => {

  try {

    gateway.checkout(req.body.fiat,
                     req.body.order,
                     req.body.customer,
                     req.body.business,
                     req.body.network,
                     function(err1, chainResult) {

      if (err1) {

        res.status(500).json({

          error: JSON.stringify(err1)

        });

      }

      gateway.getInvestmentPreferences(req.body.customer,
                                       req.body.network,
                                       function(err2, prefRes) {

        if (err2) {

          res.status(500).json({

            error: JSON.stringify(err2)

          });

        }

        res.status(200).json({

          bondedFiat: chainResult[0].toString(),
          treasuryFiat: chainResult[1].toString(),
          savingsPref: prefRes[0].toString(),
          lendingPref: prefRes[1].toString()

        });

      })

    });

  } catch(err) {

    res.status(500).json({

      error: JSON.stringify(err)

    });

  }

}

exports.shop_investment_preferences = async (req, res, next) => {

  try {

    gateway.getInvestmentPreferences(req.body.customer,
                                     req.body.network,
                                     function(err, prefRes) {

      if (err) {

        res.status(500).json({

          error: JSON.stringify(err)

        });

      }

      res.status(200).json({

        savingsPref: prefRes[0].toString(),
        lendingPref: prefRes[1].toString()

      });

    })

  } catch(err) {

    res.status(500).json({

      error: JSON.stringify(err)

    });

  }

}
