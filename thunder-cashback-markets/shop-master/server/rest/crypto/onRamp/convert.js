const rp = require('request-promise');
const keys = require('../sensible.js');

var weiConversion = 10**18

const requestOptions = {
  method: 'GET',
  uri: 'https://min-api.cryptocompare.com/data/price?api_key=' + keys.apiKey + '&fsym=ETH&tsyms=USD',
  json: true,
  gzip: true
};

async function dollarsToWei(money, callback) {

  rp(requestOptions).then(response => {

    callback(undefined, money / response.USD * weiConversion);
  }).catch((err) => {
    callback(err.message, undefined);
  });

}

async function weiToDollars(wei, callback) {

  var toEth = wei / (10**18);

  rp(requestOptions).then(response => {
    callback(undefined, toEth * response.USD);
  }).catch((err) => {
    callback(err.message, undefined);
  });

}

module.exports = {

  dollarsToWei,
  weiToDollars

}
