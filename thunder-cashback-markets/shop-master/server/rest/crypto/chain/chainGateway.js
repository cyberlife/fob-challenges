const BigNumber = require('bignumber.js');

var chainInteraction = require('./chainInteraction.js');
var utils = require('./utils.js');
var onRamp = require('../onRamp/convert.js');

var localAddresses = require('./localAddresses.js');
var thunderAddresses = require('./thunderAddresses.js');

//TODO: from the on-ramped crypto, only take into account the customer's portion
//TODO: possibly broken computations, it gives way too much bonded cashback

async function checkout(totalMoney, order, customerAddress,
                        businessAddress, network, callback) {

  //Check if the business can pay

  var businessBalance = await utils.getBalance(businessAddress, network);

  if (businessBalance == 0)
    callback("The business cannot pay for the transaction", undefined);

  var addresses, businessAddressPos = -1, businessPrivateKey = "";

  //Get the keys

  if (network == "local")
    addresses = localAddresses.addresses;

  else if (network == "thunder")
    addresses = thunderAddresses.addresses;

  for (var i = 0; i < addresses.length; i++) {

    if (addresses[i] == businessAddress) {

      businessAddressPos = i;
      businessPrivateKey = thunderAddresses.private[i]
      break;

    }

  }

  if (network == "thunder") {

    businessAddressPos = 0;
    businessPrivateKey = thunderAddresses.private[0];
    businessAddress = thunderAddresses.addresses[0];

  }

  //Convert fiat to crypto

  onRamp.dollarsToWei(totalMoney, async function(err, crypto) {

    if (err)
      callback("Could not buy tokens, aborting", undefined);

    //Get the shop contract

    var shopInstance = await utils.getContractInstance(network, "shop");

    //Get the CBT contract

    var cbtInstance = await utils.getContractInstance(network, "cbt");

    //Get the treasury instance

    var treasuryInstance = await utils.getContractInstance(network, "treasury");

    //Encrypt and sign the order

    var encryptedOrder = await utils.encryptAndSign(network, businessAddressPos, order);

    chainInteraction.calculateCurvedMintReturn(cbtInstance, businessAddress, crypto.toString(),
                                    function(returnErr, returnResult) {

      if (returnErr)
        callback("Could not get the return on invested crypto", undefined);

      chainInteraction.balanceOf(cbtInstance, businessAddress, customerAddress,
                                 function(balanceErr, balanceResult) {

        if (balanceErr)
          callback("Could not get the customer's current balance", undefined);

        chainInteraction.getTotalSupply(cbtInstance, businessAddress,
                                        function(supplyErr, supplyResult) {

          if (supplyErr)
            callback("Could not get the total supply of the token", undefined);

          chainInteraction.getPoolBalance(cbtInstance, businessAddress,
                                          function(poolErr, poolResult) {

            if (poolErr)
              callback("Could not get the pool size", undefined);

            chainInteraction.calculateSaleReturn(cbtInstance, businessAddress,
                                            ((new BigNumber(supplyResult)).plus(new BigNumber(returnResult.mintAmount))).toString(),
                                            ((new BigNumber(poolResult.toString())).plus(new BigNumber(crypto.toString()))).toString(),
                                            500000,
                                            ((new BigNumber(balanceResult)).plus(new BigNumber(returnResult.mintAmount))).toString(),
                                            function(saleErr, saleResult) {

              if (saleErr)
                callback("Could not compute the sale return", undefined);

              chainInteraction.buySomething(network, shopInstance, businessAddress,
                                            businessPrivateKey, customerAddress,
                                            encryptedOrder[0].toString(), crypto.toString(),
                                            encryptedOrder[1].v, encryptedOrder[1].r,
                                            encryptedOrder[1].s, function(buyErr, buyResult) {

                if (buyErr)
                  callback("Could not register the order on-chain and create a cashback order", undefined);

                onRamp.weiToDollars(saleResult.toString(), function (offRampError, offRampResult) {

                  if (offRampError) callback(offRampError, undefined);

                  chainInteraction.getTreasuryBalance(treasuryInstance, businessAddress,
                                                      customerAddress, function(treasuryErr, treasuryResult) {

                    if (treasuryErr) callback(treasuryErr, undefined)

                    onRamp.weiToDollars(treasuryResult.toString(), function (offRampTreasuryErr, offRampTreasuryResult) {

                      if (offRampTreasuryErr) callback(offRampTreasuryErr, undefined);

                      else callback(undefined, [parseFloat(offRampResult).toFixed(2),
                            parseFloat(offRampTreasuryResult).toFixed(2)]);

                    })

                  })

                })

              });

            })

          })

        })

      })

    })

  });

}

async function getInvestmentPreferences(customerAddress, network, callback) {

  var addresses;

  //Get the keys

  if (network == "local")
    addresses = localAddresses.addresses;

  else if (network == "thunder")
    addresses = thunderAddresses.addresses;

  var treasuryInstance = await utils.getContractInstance(network, "treasury");

  var savingsPref =
    await chainInteraction
    .getSavingsPreference(treasuryInstance, addresses[0], customerAddress);

  var lendingPref =
    await chainInteraction
    .getSavingsPreference(treasuryInstance, addresses[0], customerAddress);

  callback(undefined, [savingsPref,  lendingPref])

}

//TEST RELAYER

/*async function testShopOrder() {

  checkout(1, "Some fancy order", thunderAddresses.addresses[1],
           thunderAddresses.addresses[0], "thunder", function(err, res) {

    console.log("Off ramp:")

    if (err) console.log(err)
    else console.log(res)

  })

}

testShopOrder();*/

module.exports = {

  checkout,
  getInvestmentPreferences

}
