var ethers = require('ethers');
const EthCrypto = require('eth-crypto');

var chainInteraction = require('./chainInteraction.js');

var shopABI = require('../abi/Shop.js');
var marketsABI = require('../abi/CashbackMarkets.js');
var logicABI = require('../abi/Logic.js');
var houseABI = require('../abi/ClearingHouse.js');
var treasuryABI = require('../abi/Treasury.js');
var cbtABI = require('../abi/DetailedCBT.js');

var contractsAddr = require('./contractAddresses.js');

var localAccounts = require('./localAddresses.js');
var thunderAccounts = require('./thunderAddresses.js');

async function encryptAndSign(networkType, addrPos, shopOrder) {

  var devPrivateKeys;

  if (networkType == "local") devPrivateKeys = localAccounts;
  else devPrivateKeys = thunderAccounts;

  var accPublicKey = EthCrypto.publicKeyByPrivateKey(
    devPrivateKeys.private[addrPos]
  );

  var encryptedMsg = await EthCrypto.encryptWithPublicKey(
    accPublicKey,
    shopOrder
  );

  var stringifiedMsg = EthCrypto.cipher.stringify(encryptedMsg);

  var wallet = new ethers.Wallet(devPrivateKeys.private[addrPos]);
  var signature = await wallet.signMessage(stringifiedMsg)

  // Split the signature into its r, s and v (Solidity's format)
  var sig = await ethers.utils.splitSignature(signature);

  return [stringifiedMsg, sig];

}

async function getBalance(target, network) {

  var web3;

  web3 = await chainInteraction.getWeb3(network);

  var balance = await web3.eth.getBalance(target);

  return balance;

}

async function getContractInstance(network, contractName) {

    var web3, shopInstance, marketsInstance,
      treasuryInstance, cbtInstance, logicInstance, houseInstance;

    if (network == "local") {

      web3 = await chainInteraction.getWeb3('local');

      if (contractName == "shop") {

        try {

            shopInstance = await new web3.eth.Contract(

            shopABI.shop.abi,
            contractsAddr.localContracts[0],

            {from: localAccounts.addresses[0]}

          );

          return shopInstance;

        } catch(err) {

          return undefined;

        }

    } else if (contractName == "markets") {

      try {

          marketsInstance = await new web3.eth.Contract(

          marketsABI.markets.abi,
          contractsAddr.localContracts[4],

          {from: localAccounts.addresses[0]}

        );

        return marketsInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "logic") {

      try {

          logicInstance = await new web3.eth.Contract(

          logicABI.logic.abi,
          contractsAddr.localContracts[4],

          {from: localAccounts.addresses[0]}

        );

        return logicInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "treasury") {

      try {

          treasuryInstance = await new web3.eth.Contract(

            treasuryABI.treasury.abi,
            contractsAddr.localContracts[6],

            {from: localAccounts.addresses[0]}

        );

        return treasuryInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "house") {

      try {

          houseInstance = await new web3.eth.Contract(

            houseABI.house.abi,
            contractsAddr.localContracts[3],

            {from: localAccounts.addresses[0]}

        );

        return houseInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "cbt") {

      try {

          cbtInstance = await new web3.eth.Contract(

            cbtABI.cbt.abi,
            contractsAddr.localContracts[5],

            {from: localAccounts.addresses[0]}

        );

        return cbtInstance;

      } catch(err) {

        return undefined;

      }

    }

  } else if (network == "thunder") {

      web3 = await chainInteraction.getWeb3('thunder');

      if (contractName == "shop") {

        try {

            shopInstance = await new web3.eth.Contract(

            shopABI.shop.abi,
            contractsAddr.thunderContracts[0],

            {from: thunderAccounts.addresses[0]}

          );

          return shopInstance;

        } catch(err) {

          return undefined;

        }

    } else if (contractName == "markets") {

      try {

          marketsInstance = await new web3.eth.Contract(

          marketsABI.markets.abi,
          contractsAddr.thunderContracts[4],

          {from: thunderAccounts.addresses[0]}

        );

        return marketsInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "logic") {

      try {

          logicInstance = await new web3.eth.Contract(

          logicABI.logic.abi,
          contractsAddr.thunderContracts[4],

          {from: thunderAccounts.addresses[0]}

        );

        return logicInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "treasury") {

      try {

          treasuryInstance = await new web3.eth.Contract(

            treasuryABI.treasury.abi,
            contractsAddr.thunderContracts[6],

            {from: thunderAccounts.addresses[0]}

        );

        return treasuryInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "house") {

      try {

          houseInstance = await new web3.eth.Contract(

            houseABI.house.abi,
            contractsAddr.thunderContracts[3],

            {from: thunderAccounts.addresses[0]}

        );

        return houseInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "cbt") {

      try {

          cbtInstance = await new web3.eth.Contract(

            cbtABI.cbt.abi,
            contractsAddr.thunderContracts[5],

            {from: thunderAccounts.addresses[0]}

        );

        return cbtInstance;

      } catch(err) {

        return undefined;

      }

    }

  }

}

module.exports = {

  encryptAndSign,
  getBalance,
  getContractInstance

}
