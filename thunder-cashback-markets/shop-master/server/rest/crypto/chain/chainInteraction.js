var Web3 = require('web3');
const axios = require('axios')
const EthereumTx = require('ethereumjs-tx')

//55 Gwei
var DEFAULT_GAS_PRICE = 65000000000

var DEFAULT_GAS_LIMIT = 1900000

async function getWeb3(network) {

  if (network == "local")
    return new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

  else return new Web3(new Web3.providers.HttpProvider("https://testnet-rpc.thundercore.com:8544"));

}

//SHOP

async function buySomething(network, instance, pubKey, privateKey, customer,
                            order, moneySent, v, r, s, callback) {

  if (network == "local") {

    instance.methods.buySomething(customer, order, v, r, s).send({

      from: pubKey,
      value: moneySent,
      gas: 800000

    }, function(err, result) {

      if (err) {callback(err, undefined);}

      callback(undefined, result);

    })

  } else {

    //Submit tx on Thunder

    var web3 = await getWeb3(network, "http");

    var nonce = await web3.eth.getTransactionCount(pubKey);

    const txParams = {
      "from": pubKey,
      "to": instance.address,
      "data": instance.methods.buySomething(customer, order, v, r, s).encodeABI(),
      "value": moneySent.toString(),
      "gas": DEFAULT_GAS_LIMIT,
      "nonce": nonce,
      "chainId": 18,
      "gasPrice": DEFAULT_GAS_PRICE
    };

    try {

      web3.eth.accounts.signTransaction(txParams, '0x' + privateKey).then(signed => {

        web3.eth
        .sendSignedTransaction(signed.rawTransaction)
        .on('receipt', (receipt) => {

          callback(undefined, receipt)

        })
        .on('error', (error) => {

          console.log(error)

          callback(error, undefined)

        })

      })

    } catch(err) {}


  }

}

//HOUSE

async function getOrdersNumber(instance, pubKey, company, callback) {

  await instance.methods.getOrdersNumber(company)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

//CHARITY MARKETS

async function getTotalReturn(instance, pubKey, target, callback) {

  await instance.methods.getTotalReturn(target)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

//TREASURY

async function getTreasuryBalance(instance, pubKey, target, callback) {

  await instance.methods.getTreasuryBalance(target)
    .call({from: pubKey.toString()}, function(err, result) {

    if (err) {callback(err, undefined);}

    callback(undefined, result);

  });

}

async function getSavingsPreference(instance, pubKey, target) {

  var savingsPref = await instance.methods.getSavingsPreference(target).call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  return savingsPref;

}

async function getLendingPreference(instance, pubKey, target) {

  var lendingPref = await instance.methods.getLendingPreference(target).call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  return lendingPref;

}

//DETAILED CBT

async function balanceOf(instance, pubKey, target, callback) {

  var balance = await instance.methods.balanceOf(target).call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  callback(undefined, balance);

}

async function calculateCurvedMintReturn(instance, pubKey, amount, callback) {

  var mintReturn = await instance.methods.calculateCurvedMintReturn(amount).call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  callback(undefined, mintReturn);

}

async function calculateCurvedBurnReturn(instance, pubKey, amount, callback) {

  var burnReturn = await instance.methods.calculateCurvedBurnReturn(amount).call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  callback(undefined, burnReturn);

}

async function getTotalSupply(instance, pubKey, callback) {

  var supply = await instance.methods.totalSupply().call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  callback(undefined, supply);

}

async function calculateSaleReturn(instance, pubKey, supply,
    connectorBalance, connectorWeight, sellAmount, callback) {

  var saleReturn = await instance.methods
  .calculateSaleReturn(supply, connectorBalance, connectorWeight, sellAmount)
  .call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  callback(undefined, saleReturn);

}

async function getPoolBalance(instance, pubKey, callback) {

  var poolBalance = await instance.methods
  .getPoolBalance()
  .call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  callback(undefined, poolBalance);

}

module.exports = {

  getWeb3,

  buySomething,

  getTotalReturn,

  getOrdersNumber,

  getTreasuryBalance,

  balanceOf,
  calculateCurvedMintReturn,
  calculateCurvedBurnReturn,
  getTotalSupply,
  calculateSaleReturn,
  getPoolBalance,

  getSavingsPreference,
  getLendingPreference

}
