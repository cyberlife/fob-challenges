# CASHBACK-SHOP

Cashback shop is a simple e-commerce integration with cashback markets. The code was taken and modified from [Polymer](https://shop.polymer-project.org/).

![shop screenshot](https://user-images.githubusercontent.com/116360/39545341-c50a9184-4e05-11e8-88e0-0e1f3fa4834b.png)

## Setup
```bash
$ cd shop-master
$ npm i
$ npm start
```

## Build
```bash
$ npm run build
```

## Run the server
Open another CLI and type:

```bash
$ npm run rest-server
```
