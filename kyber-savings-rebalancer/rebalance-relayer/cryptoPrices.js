const rp = require('request-promise');
const keys = require('./sensible.js');
// Import node-fetch to query the trading API
var fetch = require('node-fetch');

const requestOneToken = {
  method: 'GET',
  uri: 'https://min-api.cryptocompare.com/data/price?api_key=' + keys.apiKey + '&fsym=',
  json: true,
  gzip: true
};

const requestMultiple = {

  method: 'GET',
  uri: 'https://min-api.cryptocompare.com/data/pricemulti?api_key=' + keys.apiKey + '&fsyms=',
  json: true,
  gzip: true

}

async function getCryptoPrice(names, fiat, callback) {

  var requestCopy

  if (names.length == 1)
    requestCopy = requestOneToken

  else
    requestCopy = requestMultiple

  for (var i = 0; i < names.length; i++) {

    requestCopy.uri = requestCopy.uri + names[i].toString()

    if (i < names.length - 1)
      requestCopy.uri = requestCopy.uri + ","

  }

  requestCopy.uri = requestCopy.uri + '&tsyms=' + fiat.toString()

  rp(requestCopy).then(response => {
    callback(undefined, response)
  }).catch((err) => {
    callback(err, undefined)
  });

}

module.exports = {

  getCryptoPrice

}
