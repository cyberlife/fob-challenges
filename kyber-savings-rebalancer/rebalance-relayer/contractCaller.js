var Web3 = require('web3');
const axios = require('axios')

var accounts = require('./accounts.js');

//71 Gwei
var DEFAULT_GAS_PRICE = 71000000000
var DEFAULT_GAS_LIMIT = 5500000

async function getWeb3(network, type) {

  if (type == undefined) return undefined;

  if (type == "http") {

    if (network == "local")
      return new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

    else if (network == "rinkeby") {

      return new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

    }

  } else {

    if (network == "local")
      return new Web3(new Web3.providers.WebsocketProvider("http://localhost:8545"));

    else if (network == "rinkeby") {

      return new Web3(new Web3.providers.WebsocketProvider("https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2"));

    }

  }

}

//SAVINGS REBALANCER

async function swapTokenToToken(network, instance, pubKey, privateKey,
    srcToken, srcQty, destToken, destQty, customer) {

  if (network == "local") {

    instance.methods
    .swapTokenToToken(srcToken, srcQty.toString(), destToken,
    destQty.toString(), customer).send({

      from: pubKey,
      gas: DEFAULT_GAS_LIMIT

    })
    .on('receipt', (receipt) => {

    })
    .on('error', (error) => {

      console.log("Could not swap token to token for " + customer);

    })

  } else {

    //Submit tx on Rinkeby

    var web3 = await getWeb3(network, "http");

    var nonce = await web3.eth.getTransactionCount(pubKey);

    const txParams = {
      "from": pubKey,
      "to": instance.address,
      "data": instance.methods
              .swapTokenToToken(srcToken, srcQty, destToken, customer)
              .encodeABI(),
      "value": "0x0",
      "gas": DEFAULT_GAS_LIMIT,
      "nonce": nonce,
      "chainId": 4,
      "gasPrice": DEFAULT_GAS_PRICE
    };

    try {

      web3.eth.accounts.signTransaction(txParams, '0x' + privateKey).then(signed => {

        web3.eth
        .sendSignedTransaction(signed.rawTransaction)
        .on('receipt', (receipt) => {

          console.log("Swapped " + srcToken + " to " + destToken + " for " + customer);

        })
        .on('error', (error) => {

          console.log("Could not swap token to token for " + customer);

        })

      })

    } catch(err) {}

  }

}

async function swapEtherToToken(network, instance, pubKey, privateKey,
    token, customer, amount, destAmount) {

  if (network == "local") {

    instance.methods
    .swapEtherToToken(token, customer, amount, destAmount.toString())
    .send({

      from: pubKey,
      gas: DEFAULT_GAS_LIMIT

    })
    .on('receipt', (receipt) => {

      console.log("Swapped" + customer + "'s ETH for " + token)

    })
    .on('error', (error) => {

      console.log("Could not swap Ether to " + token + " for " + customer);

    })

  } else {

    //Submit tx on Rinkeby

    var web3 = await getWeb3(network, "http");

    var nonce = await web3.eth.getTransactionCount(pubKey);

    const txParams = {
      "from": pubKey,
      "to": instance.address,
      "data": instance.methods
              .swapEtherToToken(token, amount, customer)
              .encodeABI(),
      "value": "0x0",
      "gas": DEFAULT_GAS_LIMIT,
      "nonce": nonce,
      "chainId": 4,
      "gasPrice": DEFAULT_GAS_PRICE
    };

    try {

      web3.eth.accounts.signTransaction(txParams, '0x' + privateKey).then(signed => {

        web3.eth
        .sendSignedTransaction(signed.rawTransaction)
        .on('receipt', (receipt) => {

          console.log("Swapped ether to " + token + " for " + customer);

        })
        .on('error', (error) => {

          console.log("Could not swap ether to " + token + " for " + customer);

        })

      })

    } catch(err) {}

  }

}

async function swapTokenToEther(network, instance, pubKey, privateKey, token,
  customer, tokenQty, etherAmount) {

  if (network == "local") {

    instance.methods.swapTokenToEther(token, customer, tokenQty.toString(), etherAmount.toString()).send({

      from: pubKey,
      gas: DEFAULT_GAS_LIMIT

    })
    .on('receipt', (receipt) => {

    })
    .on('error', (error) => {

      console.log("Could not swap " + token + " to ether for " + customer);

    })

  } else {

    //Submit tx on Rinkeby

    var web3 = await getWeb3(network, "http");

    var nonce = await web3.eth.getTransactionCount(pubKey);

    const txParams = {
      "from": pubKey,
      "to": instance.address,
      "data": instance.methods
              .swapTokenToEther(token, tokenQty, customer)
              .encodeABI(),
      "value": "0x0",
      "gas": DEFAULT_GAS_LIMIT,
      "nonce": nonce,
      "chainId": 4,
      "gasPrice": DEFAULT_GAS_PRICE
    };

    try {

      web3.eth.accounts.signTransaction(txParams, '0x' + privateKey).then(signed => {

        web3.eth
        .sendSignedTransaction(signed.rawTransaction)
        .on('receipt', (receipt) => {

          console.log("Swapped " + token + " to ether for " + customer);

        })
        .on('error', (error) => {

          console.log("Could not swap " + token + " to ether for " + customer);

        })

      })

    } catch(err) {}

  }

}

async function getCustomerBalance(network, instance, pubKey, customer, token) {

  var balance = await instance.methods.getCustomerBalance(customer, token).call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  })

  return balance

}

async function getETHAddress(network, instance, pubKey) {

  var ethAddress = await instance.methods.getETHAddress().call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  return ethAddress;

}

//SAVINGS REBALANCER DATA

async function billCustomer(network, instance, pubKey, privateKey, customer) {

  if (network == "local") {

    instance.methods.billCustomer(customer).send({

      from: pubKey,
      gas: DEFAULT_GAS_LIMIT

    })
    .on('receipt', (receipt) => {

      console.log("Billed customer with address: " + customer);

    })
    .on('error', (error) => {

      console.log("Could not bill the customer with address: " + customer);

    })

  } else {

    //Submit tx on Rinkeby

    var web3 = await getWeb3(network, "http");

    var nonce = await web3.eth.getTransactionCount(pubKey);

    const txParams = {
      "from": pubKey,
      "to": instance.address,
      "data": instance.methods
              .billCustomer(customer)
              .encodeABI(),
      "value": "0x0",
      "gas": DEFAULT_GAS_LIMIT,
      "nonce": nonce,
      "chainId": 4,
      "gasPrice": DEFAULT_GAS_PRICE
    };

    try {

      web3.eth.accounts.signTransaction(txParams, '0x' + privateKey).then(signed => {

        web3.eth
        .sendSignedTransaction(signed.rawTransaction)
        .on('error', (error) => {

          console.log("Could not bill the customer with address: " + customer);

        })
        .on('receipt', (receipt) => {

          console.log("Billed customer with address: " + customer);

        })

      })

    } catch(err) {}

  }

}

async function isCustomerInitialized(network, instance, pubKey, customer) {

  var initialized = await instance.methods.isCustomerInitialized(customer).call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  return initialized;

}

async function customerStoppedService(network, instance, pubKey, customer) {

  var serviceIsStopped = await instance.methods.customerStoppedService(customer).call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  return serviceIsStopped;

}

async function getRebalancingPause(network, instance, pubKey) {

  var pause = await instance.methods.getRebalancingPause().call({

    from: pubKey,
    gas: DEFAULT_GAS_LIMIT

  });

  return pause;

}

module.exports = {

  getWeb3,

  getCustomerBalance,
  getETHAddress,

  swapTokenToToken,
  swapEtherToToken,
  swapTokenToEther,

  isCustomerInitialized,
  customerStoppedService,
  getRebalancingPause,

  billCustomer

}
