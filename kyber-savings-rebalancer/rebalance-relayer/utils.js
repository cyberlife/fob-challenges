var rebalancerABI = require("./abi/SavingsRebalancer.js");
var rebalancerDataABI = require("./abi/SavingsRebalancerData.js");

var contractsAddresses = require('./contractsAddresses.js');
var accounts = require('./accounts.js');
var contractCaller = require('./contractCaller.js');

async function getContractInstance(network, providerType, contractName) {

  var web3, rebalancerInstance, rebalancerDataInstance;

  if (network == "local") {

    web3 = await contractCaller.getWeb3('local', providerType);

    if (contractName == "rebalancer") {

      try {

        rebalancerInstance = await new web3.eth.Contract(

          rebalancerABI.rebalancer.abi,
          contractsAddresses.localContracts[0],

          {from: accounts.local[0]}

        );

        return rebalancerInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "rebalancerData") {

      try {

        rebalancerDataInstance = await new web3.eth.Contract(

          rebalancerDataABI.rebalancerData.abi,
          contractsAddresses.localContracts[1],

          {from: accounts.local[0]}

        );

        return rebalancerDataInstance;

      } catch(err) {

        return undefined;

      }

    }

    return undefined;

  } else if (network == "rinkeby") {

    web3 = await contractCaller.getWeb3('rinkeby', providerType);

    if (contractName == "rebalancer") {

      try {

          rebalancerInstance = await new web3.eth.Contract(

            rebalancerABI.rebalancer.abi,
            contractsAddresses.rinkebyContracts[0],

            {from: accounts.rinkeby[0]}

        );

        return rebalancerInstance;

      } catch(err) {

        return undefined;

      }

    } else if (contractName == "rebalancerData") {

      try {

        rebalancerDataInstance = await new web3.eth.Contract(

          rebalancerDataABI.rebalancerData.abi,
          contractsAddresses.rinkebyContracts[1],

          {from: accounts.rinkeby[0]}

        );

        return rebalancerDataInstance;

      } catch(err) {

        return undefined;

      }

    }

  }

}

module.exports = {

  getContractInstance

}
