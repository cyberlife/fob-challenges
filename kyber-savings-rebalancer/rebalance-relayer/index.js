var utils = require("./utils.js");
var contractCaller = require("./contractCaller.js");
var cryptoPrices = require("./cryptoPrices.js");
var kyberTradingAPI = require("./kyberTradingAPI.js");
var accounts = require("./accounts.js");

//ETH, KNC, MANA, ZILL, OMG

var kyberRinkebyTokens = [

  "0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
  "0x6FA355a7b6bD2D6bD8b927C489221BFBb6f1D7B2",
  "0x725d648E6ff2B8C44c96eFAEa29b305e5bb1526a",
  "0x405A656Dc1b672800D21a15eF5539D4776F6654c",
  "0x732fBA98dca813C3A630b53a8bFc1d6e87B1db65"

];

var kyberMainTokens = [

  "0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
  "0xdd974d5c2e2928dea5f71b9825b8b646686bd200",
  "0x0f5d2fb29fb7d3cfee444a200298f468908cc942",
  "0xd26114cd6ee289accf82350c8d8487fedb8a0c07",
  "0x05f4a42e251f2d52b8ed15e9fedaacfcef1fad27"

];

var customers = [

  "0x5581364f1350B82Ed4E25874f3727395BF6Ce490"

];

var latestPrices = [

  0,0,0,0,0

];

var minPriceChange = 0.01;
var toSwap = 1000;

async function start(network) {

  var rebalancerInstance =
    await utils.getContractInstance(network, "http", "rebalancer");

  var balances = await getCustomerBalances(network);

  cryptoPrices.getCryptoPrice(['ETH', 'KNC', 'MANA', 'ZIL', 'OMG'], "USD",
    async function(err, res) {

    if (latestPrices[0] == 0) {

      console.log("KYBER-REBALANCER: Fetched prices for the first time")

      setLatestPrices(res);

      setTimeout(start, 15000, "rinkeby");

    } else {

      var swapped = await getSwappedCoins(res)

      var smallestChange = await getStableCoin(res);

      initiateSwappings(balances, network, swapped, smallestChange, rebalancerInstance);

      //Because of tx underpricing in ethereum when past txs are still in the mempool, we should avoid billing regularly in the demo

    //  billAllCustomers(network);

      setLatestPrices(res);

      setTimeout(start, 600000, "rinkeby");

    }

  })

}

async function billAllCustomers(network) {

  var rebalancerData =
    await utils.getContractInstance(network, "http", "rebalancerData");

  for (var i = 0; i < customers.length; i++) {

    await contractCaller
    .billCustomer(network, rebalancerData, accounts.rinkeby[0],
                  accounts.rinkebyPrivate[0],
                  customers[i])

  }

}

async function setLatestPrices(pricesObj) {

  var prices = Object.values(pricesObj);

  for (var i = 0; i < prices.length; i++) {

    latestPrices[i] = prices[i].USD;

  }

}

async function getSwapType(smallestChange, targetToSwap) {

  if (targetToSwap == smallestChange) return "NOTHING";

  else if (targetToSwap > 0 && smallestChange == 0) return "TOKEN-ETH";

  else if (targetToSwap == 0 && smallestChange > 0) return "ETH-TOKEN";

  else if (targetToSwap > 0 && smallestChange > 0)  return "TOKEN-TOKEN";

}

async function getStableCoin(pricesObj) {

  var prices = Object.values(pricesObj);

  var stable = 0;
  var mainDifference = Math.abs(prices[0].USD - latestPrices[0]);

  for (var i = 1; i < prices.length; i++) {

    if (Math.abs(prices[i].USD - latestPrices[i]) < mainDifference) {

      mainDifference = Math.abs(prices[i].USD - latestPrices[i]);
      stable = i;

    }

  }

  return stable;

}

async function getCustomerBalances(network) {

  var balances = [];
  var currentBalances = [];

  var specificTokenBalance = 0;

  var rebalancerInstance =
  await utils.getContractInstance(network, "http", "rebalancer");

  for (var i = 0; i < customers.length; i++) {

    for (var j = 0; j < kyberRinkebyTokens.length; j++) {

      specificTokenBalance =
      await contractCaller
      .getCustomerBalance(network, rebalancerInstance, customers[0],
        customers[i], kyberRinkebyTokens[j]);

      currentBalances.push(specificTokenBalance);

    }

    balances.push(currentBalances.concat());

    currentBalances.length = 0;

    currentBalances = [];

  }

  return balances;

}

async function initiateSwappings(balances, network,
  swapped, smallestChange, rebalancerInstance) {

  var swapType = ''

  var j;

  var swapAmount = 0;

  var alreadyBeingSwapped = [];

  for (var m = 0; m < customers.length; m++) {

    alreadyBeingSwapped.push(0);

  }

  for (var i = 0; i < swapped.length; i++) {

    if (swapped[i] == 1) {

      swapType = await getSwapType(smallestChange, i);

      if (swapType == 'TOKEN-ETH') {

        for (j = 0; j < balances.length; j++) {

          if (balances[j][i] > 0 && alreadyBeingSwapped[j] == 0) {

            if (balances[j][i] > toSwap) {

              swapAmount = toSwap;

            } else {

              swapAmount = balances[j][i];

            }

            alreadyBeingSwapped[j] = 1;

            contractCaller
            .swapTokenToEther(network, rebalancerInstance,
                              accounts.rinkeby[0], accounts.rinkebyPrivate[0],
                              kyberRinkebyTokens[i], customers[j],
                              swapAmount, "0")

          }

        }

      } else if (swapType == "ETH-TOKEN") {

        for (j = 0; j < balances.length; j++) {

          if (balances[j][i] > 0 && alreadyBeingSwapped[j] == 0) {

            if (balances[j][i] > toSwap) {

              swapAmount = toSwap;

            } else {

              swapAmount = balances[j][i];

            }

            alreadyBeingSwapped[j] = 1;

            contractCaller
            .swapEtherToToken(network, rebalancerInstance,
                              accounts.rinkeby[0], accounts.rinkebyPrivate[0],
                              kyberRinkebyTokens[smallestChange], customers[j],
                              swapAmount, "0")

          }

        }

      } else if (swapType == "TOKEN-TOKEN") {

        for (j = 0; j < balances.length; j++) {

          if (balances[j][i] > 0 && alreadyBeingSwapped[j] == 0) {

            if (balances[j][i] > toSwap) {

              swapAmount = toSwap;

            } else {

              swapAmount = balances[j][i];

            }

            alreadyBeingSwapped[j] = 1;

            contractCaller
            .swapTokenToToken(network, rebalancerInstance,
                              accounts.rinkeby[0], accounts.rinkebyPrivate[0],
                              kyberRinkebyTokens[i], swapAmount, kyberRinkebyTokens[j],
                              "0", customers[j])

          }

        }

      }

    }

  }

}

async function getSwappedCoins(pricesObj) {

  var prices = Object.values(pricesObj);

  var swapped = [0,0,0,0,0];

  for (var i = 0; i < prices.length; i++) {

    if (Math.abs(prices[i].USD - latestPrices[i]) >
        latestPrices[i] * minPriceChange / 100) {

      swapped[i] = 1;

    }

  }

  return swapped;

}

start("rinkeby")
