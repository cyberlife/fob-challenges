const fetch = require('node-fetch')
const BigNumber = require('bignumber.js')
var contractCaller = require("./contractCaller.js");

//eth ==> token

async function getSellRates(id, qty) {

  let ratesRequest = await fetch('https://api.kyber.network/sell_rate?id=' + id + '&qty=' + qty)
  let rates = await ratesRequest.json()

  return rates.data[0].dst_qty

}

//token ==> eth

async function getBuyRates(id, qty) {

  let ratesRequest = await fetch('https://api.kyber.network/buy_rate?id=' + id + '&qty=' + qty)
  let rates = await ratesRequest.json()
  return rates.data[0].src_qty[0]

}

async function ethToToken(network, rebalancerInstance, localTokenAddress, mainTokenAddress,
                          customer, ethQuantity, callerAddress) {

  var tokenAmount = await getSellRates(mainTokenAddress, ethQuantity)

  tokenAmount = new BigNumber(Math.floor(tokenAmount * 10**18))

  await contractCaller.swapEtherToToken(network, rebalancerInstance, callerAddress,
                                        "privateKey",
                                        localTokenAddress, customer, ethQuantity,
                                        tokenAmount)

}

async function tokenToEth(network, rebalancerInstance, localTokenAddress, mainTokenAddress,
                          customer, tokenAmount, callerAddress) {

  var ethQuantity = await getBuyRates(mainTokenAddress, tokenAmount);

  ethQuantity = new BigNumber(Math.floor(ethQuantity * 10**18))

  await contractCaller.swapTokenToEther(network, rebalancerInstance, callerAddress,
                                        "privateKey",
                                        localTokenAddress, customer, tokenAmount,
                                        ethQuantity)

}

async function tokenToToken(network, rebalancerInstance, sourceTokenAddress,
                            sourceAmount, destTokenAddress, customer, callerAddress, mainSource, mainDest) {

  var ethQuantity = await getBuyRates(mainSource, sourceAmount);

  var destAmount = await getSellRates(mainDest, ethQuantity);

  destAmount = new BigNumber(Math.floor(destAmount * 10**18))

  await contractCaller.swapTokenToToken(network, rebalancerInstance, callerAddress,
                                        "privateKey",
                                        sourceTokenAddress, sourceAmount,
                                        destTokenAddress, destAmount,
                                        customer)

}

module.exports = {

  getSellRates,
  getBuyRates,

  ethToToken,
  tokenToEth,
  tokenToToken

}
