var SavingsRebalancer =
  artifacts.require("contracts/cyber-integration/SavingsRebalancer.sol");

var SavingsRebalancerData =
  artifacts.require("contracts/cyber-integration/SavingsRebalancerData.sol");

var DAO = artifacts.require("contracts/cyber-integration/DAO.sol");

require('dotenv').config();

module.exports = async function (deployer, network, accounts) {

  //KyberNetworkProxy, KNC, MANA, ZILL, OMG

  var rinkebyKyber = [

    "0xF77eC7Ed5f5B9a5aee4cfa6FFCaC6A4C315BaC76",
    "0x6FA355a7b6bD2D6bD8b927C489221BFBb6f1D7B2",
    "0x725d648E6ff2B8C44c96eFAEa29b305e5bb1526a",
    "0x405A656Dc1b672800D21a15eF5539D4776F6654c",
    "0x732fBA98dca813C3A630b53a8bFc1d6e87B1db65"

  ];

  var rinkebyRelayers = [];

  var localRelayers = [

    accounts[1]

  ];

  var rinkebyMain = "0x5581364f1350B82Ed4E25874f3727395BF6Ce490";

  //Rebalancer data params
  var maxPreferredCoins = 5;
  var rebalancingPause = 600000; //600 seconds
  var baseFee = 50; //wei
  var minServiceDuration = 140; //seconds
  var initialCustomerDeposit = "300000000000000000" //0.3 ETH

  if (network == 'rinkeby') {

    deployer.deploy(SavingsRebalancer, {from: rinkebyMain}).then(function() {
    return deployer.deploy(SavingsRebalancerData, {from: rinkebyMain}).then(function() {
    return deployer.deploy(DAO, {from: rinkebyMain}).then(async function() {

      var savingsRebalancer = await SavingsRebalancer.deployed();
      var rebalancerData = await SavingsRebalancerData.deployed();
      var dao = await DAO.deployed();

      //SavingsRebalancerData setup
      await rebalancerData.changeRebalancerAddress(savingsRebalancer.address,
          {from: rinkebyMain});

      await rebalancerData.changeMinRebalancingPause(rebalancingPause,
        {from: rinkebyMain});

      await rebalancerData.changeCyberFund(dao.address, {from: rinkebyMain});
      await rebalancerData.changeBaseFee(baseFee, {from: rinkebyMain});

      await rebalancerData.changeDiversificationMultiplier(0, 100, {from: rinkebyMain});
      await rebalancerData.changeDiversificationMultiplier(1, 125, {from: rinkebyMain});
      await rebalancerData.changeDiversificationMultiplier(2, 150, {from: rinkebyMain});

      await rebalancerData.changeMinServiceDuration(minServiceDuration, {from: rinkebyMain});

      await rebalancerData.initializeCustomer(0, {from: rinkebyMain})

      //Store fee money

      await rebalancerData.depositFees({from: rinkebyMain, value: initialCustomerDeposit})

      //SavingsRebalancer setup

      await savingsRebalancer.setKyberProxy(rinkebyKyber[0], {from: rinkebyMain});
      await savingsRebalancer
        .changeRebalancerData(rebalancerData.address, {from: rinkebyMain});

      //Start service for customer

      await rebalancerData.startService({from: rinkebyMain});

      //Exchange coins

      await savingsRebalancer.depositETH({from: rinkebyMain, value: 1000000000000000000})

      await savingsRebalancer
        .swapEtherToToken(rinkebyKyber[1], 100000000, rinkebyMain,
          {from: rinkebyMain})

      var kncBalance =
      await savingsRebalancer
      .getCustomerBalance(rinkebyMain, rinkebyKyber[1])

      console.log("KNC balance: " + kncBalance.toString())

    }) }) })

  } else if (network == "development") {

    deployer.deploy(SavingsRebalancer, {from: accounts[0]}).then(function() {
    return deployer.deploy(SavingsRebalancerData, {from: accounts[0]}).then(function() {
    return deployer.deploy(DAO, {from: accounts[0]}).then(async function() {

      var savingsRebalancer = await SavingsRebalancer.deployed();
      var rebalancerData = await SavingsRebalancerData.deployed();
      var dao = await DAO.deployed();

      //SavingsRebalancerData setup
      await rebalancerData.changeRebalancerAddress(savingsRebalancer.address,
          {from: accounts[0]});

      await rebalancerData.changeMinRebalancingPause(rebalancingPause,
        {from: accounts[0]});

      await rebalancerData.changeCyberFund(dao.address, {from: accounts[0]});
      await rebalancerData.changeBaseFee(baseFee, {from: accounts[0]});

      await rebalancerData.changeDiversificationMultiplier(0, 100, {from: accounts[0]});
      await rebalancerData.changeDiversificationMultiplier(1, 125, {from: accounts[0]});
      await rebalancerData.changeDiversificationMultiplier(2, 150, {from: accounts[0]});

      await rebalancerData.changeMinServiceDuration(minServiceDuration, {from: accounts[0]});

      for (var i = 0; i < localRelayers.length; i++) {

        await rebalancerData.toggleRelayer(localRelayers[i], {from: accounts[0]});

      }

      await rebalancerData.initializeCustomer(0, {from: accounts[0]})

      //Store fee money

      await rebalancerData.depositFees({from: accounts[0], value: initialCustomerDeposit})

      //SavingsRebalancer setup

      await savingsRebalancer.setKyberProxy(accounts[1], {from: accounts[0]});

      await savingsRebalancer
        .changeRebalancerData(rebalancerData.address, {from: accounts[0]});

      //Start service for customer

      await rebalancerData.startService({from: accounts[0]});

      //Exchange coins

      await savingsRebalancer.depositETH({from: accounts[0], value: 100000000000})

      //Bill customer

      await rebalancerData.billCustomer(accounts[0], {from: accounts[0]});

    }) }) })

  }

}
