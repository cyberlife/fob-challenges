pragma solidity 0.4.25;

contract ISavingsRebalancerData {

  event ChangedMaxPreferred(uint256 max);

  event ChangedRebalancerAddress(address rebalancer);

  event ChangedMinRebalancingPause(uint256 pause);

  event ChangedCyberFund(address fund);

  event ChangedBaseFee(uint256 fee);

  event ChangedDiversificationMultiplier(uint256 level, uint256 multiplier);

  event ChangedMinServiceDuration(uint256 duration);

  event ToggledRelayer(address relayer);

  event CustomerInitialized(address indexed target, uint256 diversification);

  event ChangedSetup(address target, uint256 diversification);

  event DepositedFees(address target, uint256 amount);

  event WithdrewFees(address target, uint256 amount);

  event StartedService(address target, uint256 endDate);

  event StoppedService(address target, uint256 remainingMoney);

  event BilledCustomer(address customer, address relayer, uint256 money);

  event UpdatedCoinsAmount(address relayer, address customer, string distribution, uint256 coinsAmount);


  function changeRebalancerAddress(address _reb) public;

  function changeMinRebalancingPause(uint256 _pause) public;

  function changeCyberFund(address _fund) public;

  function changeBaseFee(uint256 fee) public;

  function changeDiversificationMultiplier(uint256 diverseLevel, uint256 multiplier)
    public;

  function changeMinServiceDuration(uint256 _minServiceDuration) public;

  function toggleRelayer(address _relayer) public;

  function initializeCustomer(uint256 diversification)
    public;

  function changeSetup(uint256 diversification)
    public;

  function depositFees() public payable;

  function withdrawFees(uint256 money)
    public;

  function startService() public;

  function stopService() public;

  function updateCoinAmount(address customer, uint256 coins, string memory distributionList)
    public;

  function billCustomer(address customer)
    public;

  function isCustomerInitialized(address customer) public view returns (bool);

  function customerCoinList(address customer) public view returns (string memory);

  function customerDiversificationLevel(address customer) public view returns (uint256);

  function customerCurrentCoinNumber(address customer) public view returns (uint256);

  function customerRemainingFeeMoney(address customer) public view returns (uint256);

  function customerCheckpointedPayment(address customer) public view returns (uint256);

  function customerServiceStart(address customer) public view returns (uint256);

  function customerStoppedService(address customer) public view returns (bool);

  function customerBlockedRequests(address customer) public view returns (bool);

  function timeUntilStop(address customer) public view returns (int256);

  function serviceTimeForCurrentFunds(address customer) public view returns (uint256);

  function setupPricePerSecond(uint256 diverse, uint256 coinsNumber)
    public view returns (uint256);

  function customerPricePerSecond(address customer) public view returns (uint256);

  function enoughMoneyForMinimumService(address customer) public view returns (bool);

  function isRelayerApproved(address relayer) public view returns (bool);

  function getRebalancingPause() public view returns (uint256);

}
