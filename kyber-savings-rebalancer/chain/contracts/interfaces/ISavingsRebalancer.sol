pragma solidity 0.4.25;

import "contracts/kyber-core/ERC20Interface.sol";

contract ISavingsRebalancer {

  event ExchangedTokens(address customer, address sourceToken,
    address desToken, uint256 sourceQty, uint256 destQty);

  event ChangedKyberProxy(address proxy);

  event ChangedWhitelisted(address _white);

  event ChangedRebalancerData(address _data);

  event DepositedETH(address customer, uint256 amount);

  event WithdrewETH(address customer, uint256 amount);

  event DepositedToken(address customer, address token, uint256 amount);

  event WithdrewToken(address customer, address token, uint256 amount);


  function setKyberProxy(address _proxy) public;

  function depositETH() public payable;

  function withdrawETH(uint256 amount)
    public;

  function swapTokenToToken (
    ERC20 srcToken,
    uint srcQty,
    ERC20 destToken,
    address customer
  ) public;

  function swapEtherToToken(
    ERC20 token,
    uint amount,
    address customer)
    public;

  function swapTokenToEther (
    ERC20 token,
    uint tokenQty,
    address customer)
    public;

  function getCustomerBalance(address customer, address token)
    public view returns (uint256);

  function getTotalBalance(address customer) public view returns (uint256);

  function getETHAddress() public view returns (address);

}
