pragma solidity 0.4.25;

contract ContractDetector {

  function isContract(address _addr) public returns (bool isContract) {

    uint32 size;

    assembly {
      size := extcodesize(_addr)
    }

    return (size > 0);

  }

}
