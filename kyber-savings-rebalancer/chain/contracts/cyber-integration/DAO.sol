pragma solidity 0.4.25;

import "contracts/zeppelin/ownership/Ownable.sol";

contract DAO is Ownable {

  function DAO() public {}

  function() external payable {}

  function transferFunds(uint256 amount, address target) public onlyOwner {

    target.transfer(amount);

  }

}
