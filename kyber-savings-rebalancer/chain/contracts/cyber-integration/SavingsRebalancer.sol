pragma solidity 0.4.25;

import "contracts/kyber-core/KyberNetworkProxyInterface.sol";
import "contracts/zeppelin/ownership/Ownable.sol";

import "contracts/zeppelin/SafeMath.sol";

import "contracts/interfaces/ISavingsRebalancer.sol";
import "contracts/interfaces/ISavingsRebalancerData.sol";

import "contracts/zeppelin/ContractDetector.sol";

contract SavingsRebalancer is ISavingsRebalancer, Ownable, ContractDetector {

    using SafeMath for uint256;
    using SafeMath for int256;

    //It is possible to take minRate from kyber contract, but best to get it as an input from the user.

    mapping(address => mapping(address => uint256)) tokenBalances;
    mapping(address => uint256) totalBalances;
    mapping(address => bool) portfolioIsBeingModified;

    ERC20 constant internal ETH_TOKEN_ADDRESS = ERC20(0x00eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee);

    KyberNetworkProxyInterface _kyberNetworkProxy;
    ISavingsRebalancerData rebalancerData;

    modifier canTradeForCustomer(address customer) {

      require(

        totalBalances[customer] > 0 &&
        portfolioIsBeingModified[customer] == false,

        "Cannot trade for the customer"

      );

      _;

    }

    modifier customerStartedService(address customer) {

      require(rebalancerData.isCustomerInitialized(customer) == true &&
              rebalancerData.customerStoppedService(customer) == false,

            "The customer did not request the rebalancing service"

            );

      _;

    }

    modifier onlyAllowedRelayer() {

      require(

        rebalancerData.isRelayerApproved(msg.sender) == true,

        "This relayer is not approved"

      );

      _;

    }

    modifier portfolioIsNotModified(address customer) {

      require(
        portfolioIsBeingModified[customer] == false,
        "The customer has their portfolio under modification"
      );

      _;

    }

    function() public payable {}

    function setKyberProxy(address _proxy) public onlyOwner {

      _kyberNetworkProxy = KyberNetworkProxyInterface(_proxy);

      ChangedKyberProxy(_proxy);

    }

    function changeRebalancerData(address _data) public onlyOwner {

      rebalancerData = ISavingsRebalancerData(_data);

      ChangedRebalancerData(_data);

    }

    function depositETH() public payable portfolioIsNotModified(msg.sender)
    {

      require(msg.value > 0, "You need to send more than 0 wei");

      tokenBalances[msg.sender][address(ETH_TOKEN_ADDRESS)] =
        tokenBalances[msg.sender][address(ETH_TOKEN_ADDRESS)].add(msg.value);

      totalBalances[msg.sender] = totalBalances[msg.sender].add(msg.value);

      DepositedETH(msg.sender, msg.value);

    }

    function withdrawETH(uint256 amount)
      public portfolioIsNotModified(msg.sender)
    {

      require(tokenBalances[msg.sender][address(ETH_TOKEN_ADDRESS)] >= amount);

      tokenBalances[msg.sender][address(ETH_TOKEN_ADDRESS)] =
        tokenBalances[msg.sender][address(ETH_TOKEN_ADDRESS)].sub(amount);

      totalBalances[msg.sender] = totalBalances[msg.sender].sub(amount);

      msg.sender.transfer(amount);

      WithdrewETH(msg.sender, amount);

    }

    //@param srcToken source token contract address
    //@param srcQty in token wei
    //@param destToken destination token contract address
    //@param destAddress address to send swapped tokens to
    function swapTokenToToken (
      ERC20 srcToken,
      uint srcQty,
      ERC20 destToken,
      address customer
    ) public
      portfolioIsNotModified(customer)
      customerStartedService(customer)
      onlyAllowedRelayer {

        require(tokenBalances[customer][srcToken] >= srcQty,
          "The customer does not have that many source tokens");

        //Block other portfolio modifications until this one is done

        portfolioIsBeingModified[customer] = true;

        //Decrease customer's balance for source token

        tokenBalances[customer][srcToken] =
          tokenBalances[customer][srcToken].sub(srcQty);

        totalBalances[customer] = totalBalances[customer].sub(srcQty);

        uint minRate;

        //getExpectedRate returns expected rate and slippage rate
        //we use the slippage rate as the minRate
        (, minRate) = _kyberNetworkProxy.getExpectedRate(srcToken, destToken, srcQty);

        // Mitigate ERC20 Approve front-running attack, by initially setting
        // allowance to 0
        require(srcToken.approve(address(_kyberNetworkProxy), 0), "Could not prevent frontrunning");

        // Approve tokens so network can take them during the swap
        srcToken.approve(address(_kyberNetworkProxy), srcQty);

        uint destAmount = _kyberNetworkProxy.swapTokenToToken(srcToken, srcQty, destToken, minRate);

        //Increment balance of customer for traded token

        tokenBalances[customer][destToken] =
          tokenBalances[customer][destToken].add(destAmount);

        totalBalances[customer] = totalBalances[customer].add(destAmount);

        //Unlock for modifications

        portfolioIsBeingModified[customer] = false;

        //Emit event

        ExchangedTokens(customer, address(srcToken),
          address(destToken), srcQty, destAmount);

    }

    //@dev assumed to be receiving ether wei
    //@param token destination token contract address
    //@param destAddress address to send swapped tokens to
    function swapEtherToToken(
      ERC20 token,
      uint amount,
      address customer)
      public
    //  portfolioIsNotModified(customer)
    //  customerStartedService(customer)
    //  onlyAllowedRelayer
      {

        require(amount > 0, "The amount is not greater than zero");

        require(tokenBalances[customer][address(ETH_TOKEN_ADDRESS)] >= amount,
          "The customer does not have that much ETH in the contract");

        //Block other portfolio modifications until this one is done

        portfolioIsBeingModified[customer] = true;

        //Decrease customer's balance for source token

        tokenBalances[customer][address(ETH_TOKEN_ADDRESS)] =
          tokenBalances[customer][address(ETH_TOKEN_ADDRESS)].sub(amount);

        totalBalances[customer] = totalBalances[customer].sub(amount);

        uint minRate;
        (, minRate) = _kyberNetworkProxy.getExpectedRate(ETH_TOKEN_ADDRESS, token, amount);

        //will send back tokens to this contract's address
        uint destAmount = _kyberNetworkProxy.swapEtherToToken.value(amount)(token, minRate);

        //Increment balance of customer for traded token

        tokenBalances[customer][token] =
          tokenBalances[customer][token].add(destAmount);

        totalBalances[customer] = totalBalances[customer].add(destAmount);

        //Unlock for modifications

        portfolioIsBeingModified[customer] = false;

        //Emit event

        ExchangedTokens(customer, address(ETH_TOKEN_ADDRESS),
          address(token), amount, destAmount);

    }

    //@param token source token contract address
    //@param tokenQty token wei amount
    //@param destAddress address to send swapped ETH to
    function swapTokenToEther (
      ERC20 token,
      uint tokenQty,
      address customer)
      public
      portfolioIsNotModified(customer)
      customerStartedService(customer)
      onlyAllowedRelayer {

        require(tokenBalances[customer][address(token)] >= tokenQty,
          "The customer that does have that many source tokens");

        //Block other portfolio modifications until this one is done

        portfolioIsBeingModified[customer] = true;

        //Decrease customer's balance for source token

        tokenBalances[customer][token] =
          tokenBalances[customer][token].sub(tokenQty);

        totalBalances[customer] = totalBalances[customer].sub(tokenQty);

        uint minRate;
        (, minRate) = _kyberNetworkProxy.getExpectedRate(token, ETH_TOKEN_ADDRESS, tokenQty);

        // Mitigate ERC20 Approve front-running attack, by initially setting
        // allowance to 0
        require(token.approve(address(_kyberNetworkProxy), 0),
          "Could not prevent frontrunning");

        // Approve tokens so network can take them during the swap
        token.approve(address(_kyberNetworkProxy), tokenQty);

        uint destAmount = _kyberNetworkProxy.swapTokenToEther(token, tokenQty, minRate);

        //Increment balance of customer for traded token

        tokenBalances[customer][address(ETH_TOKEN_ADDRESS)] =
          tokenBalances[customer][address(ETH_TOKEN_ADDRESS)].add(destAmount);

        totalBalances[customer] = totalBalances[customer].add(destAmount);

        //Unlock for modifications

        portfolioIsBeingModified[customer] = false;

        //Emit event

        ExchangedTokens(customer, address(token), address(ETH_TOKEN_ADDRESS),
          tokenQty, destAmount);

    }

    //GETTERS

    function getCustomerBalance(address customer, address token) public view returns (uint256) {

      return tokenBalances[customer][token];

    }

    function getTotalBalance(address customer) public view returns (uint256) {

      return totalBalances[customer];

    }

    function getETHAddress() public view returns (address) {

      return address(ETH_TOKEN_ADDRESS);

    }

}
