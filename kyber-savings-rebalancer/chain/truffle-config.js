var HDWalletProvider = require("truffle-hdwallet-provider");

require('dotenv').config();

module.exports = {

  networks: {

    development: {
      host: "localhost",
      port: 8545,
      network_id: "*",
      from: "0x6259ac218eed8caf47e26246d7e13c1df70165f2",
      gas: 7590000
    },

    ropsten: {

      provider: function() {
        return new HDWalletProvider(process.env.mnemonic,
            "https://ropsten.infura.io/OPVe8KJGtYczgs2np3R2")
      },
      from: "0x5581364f1350B82Ed4E25874f3727395BF6Ce490",
      network_id: 3,
      gas: 7000000,
      gasPrice: 50000000000

    },

    rinkeby: {
        provider: function() {
          return new HDWalletProvider(process.env.mnemonic,
              "https://rinkeby.infura.io/OPVe8KJGtYczgs2np3R2")
        },
        from: "0x5581364f1350B82Ed4E25874f3727395BF6Ce490",
        network_id: 4,
        gas: 6500000,
        gasPrice: 50000000000
    }

  },

  mocha: {
    enableTimeouts: false
  },

  compilers: {
    solc: {
      version: "0.4.25",
    },
  },

  solc: {
    optimizer: { // Turning on compiler optimization that removes some local variables during compilation
      enabled: true,
      runs: 200
    }
  }

};
